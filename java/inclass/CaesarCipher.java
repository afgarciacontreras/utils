import java.io.*;
import java.util.*;

public class CaesarCipher {

	private static char encodeCharCaesar(char c, int shift) {
		// Shift should always be between 0-26
		int finalShift = shift % 26;
		// Character should always be UPPERCASE
		c = Character.toUpperCase(c);
		// The ASCII for A is 65, so
		// to get the character ID such that 
		// A = 0, B = 1, etc., we do c - 65
		int charID = c - 65;
		// After that, we change the ID by shift
		charID = charID + finalShift;
		// However, if the ID is greater than 26,
		// it needs to wrap around to the beginning
		// of the alphabet, so we use the remainder
		charID = charID % 26;
		
		// We need to conver it to the actual character,
		// so we add back the 65 we took earlier,
		// cast to and assign it to a char variable
		char finalChar = (char)(charID + 65);
		
		return finalChar;
	}
	
	private static char decodeCharCaesar(char c, int shift) {
		return encodeCharCaesar(c, -shift);
	}
	
	private static String encodeStringCaesar(String s, int shift) {
		String encodedString = "";
		
		// Encode each character by the shift parameter
		for(int i = 0; i < s.length(); i++) {
			encodedString += encodeCharCaesar(s.charAt(i), shift);
		}
		
		return encodedString;
	}
	
	private static String decodeStringCaesar(String s, int shift) {
		return encodeStringCaesar(s, -shift);
	}
	
	public static void main(String[] args)  throws FileNotFoundException, IOException {
		Scanner scnr = new Scanner(System.in);
		System.out.println("Welcome to the Caesar Cipher Encoder/Decoder!");
		System.out.println("To know more about the Cipher, see:");
		System.out.println("https://en.wikipedia.org/wiki/Caesar_cipher");
		System.out.println();
		System.out.println("Do you want to Encrypt or Decrypt?");
		System.out.println("Type [1] to Encrypt, [2] to Decrypt.");
		int userInput = scnr.nextInt();
		scnr.nextLine();
		if(userInput == 1){
			System.out.print("Enter the plaintext phrase you want to Encrypt: ");
			String plaintext = scnr.nextLine();
			System.out.print("Enter the shift you want to use: ");
			int shift = scnr.nextInt();
			String cipher = encodeStringCaesar(plaintext, shift);
			System.out.println("The encrypted cipher is: ");
			System.out.println(cipher);
		} else if(userInput == 2) {
			System.out.print("Enter the cipher you want to Decrypt: ");
			String cipher = scnr.nextLine();
			System.out.print("Enter the shift you want to use: ");
			int shift = scnr.nextInt();
			String plaintext = decodeStringCaesar(cipher, shift);
			System.out.println("The decrypted plaintext phrase is: ");
			System.out.println(plaintext);
		} else {
			System.out.println("Input error. Exiting program.");
		}
		
	}

}