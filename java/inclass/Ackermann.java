import java.io.*;
import java.util.*;

public class Ackermann {
	public static int ackermann(int m, int n) {
		if(m == 0) {
			return n + 1;
		} else{
			if(n==0) {
				return ackermann(m - 1, 1);
			} else {
				return ackermann(m - 1, ackermann(m, n - 1));
			}
		}
	}
	
	public static void main(String[] args) {
		Scanner scnr = new Scanner(System.in);
		System.out.println("Welcome to the Ackermann function tester!");
		System.out.println("Enter the values for m and n separated by a space:");
		int m = scnr.nextInt();
		int n = scnr.nextInt();
		System.out.print("The value of the Ackermann function for those parameters is: ");
		System.out.println("" + ackermann(m, n));
	}
}