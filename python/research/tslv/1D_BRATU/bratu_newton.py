# -*- coding: utf-8 -*-
"""
Created on Wed Jul 11 12:26:15 2018

@author: Angel Garcia
"""
import os
import math
import scipy.optimize as opt
import numpy as np
import matplotlib.pyplot as pyplot

# TODO: Change the equations, u(0) and u(1) have boundary conds
# (( this means they do not go into x, but go into args instead ))

calls = 0
jacobian_m = np.zeros(1)

'''
Discretization of Bratu 1D function
System of equations
'''
def f(x, args):
    global calls
    n = len(x)
    y = np.zeros(n)
    #calls += 1
    #print(calls)
    #print(x)
    for i in range(n):
        try:
            y[i] = f_i_central(x, i, args)
        except Exception as e:
            #print(i)
            #print(x[i])
            #print(math.exp(x[i]))
            raise e
    #print(y)
    return y

def jacobian(x, args):
    global jacobian_m
    n = len(x)
    for i in range(n):
        jacobian_m[i,i] = (-2.0/(args[3]**2) + args[2] * math.exp(x[i]))
    return jacobian_m

'''
Discretization of Bratu 1D function
Single equation
args:
    [0] = u(0)
    [1] = u(1)
    [2] = r
    [3] = h
'''
def f_i_central(x, i, args):
    if i == 0:
        return ((x[1] - 2*x[0] + args[0])/math.pow(args[3],2)
                + args[2] * math.exp(x[0]))
    elif i == (len(x)-1):
        return ((args[1] - 2*x[i] + x[i-1])/math.pow(args[3],2)
                + args[2] * math.exp(x[i]))
    else:
        return ((x[i+1] - 2*x[i] + x[i-1])/math.pow(args[3],2)
                + args[2] * math.exp(x[i]))

def bratu_tick_format(value, tick_num):
    return "u({0:.1f})".format(value)

def plot_bratu(x, args):
    h = args[3]
    #steps = len(x) + 2
    t_series = np.arange(0.0, 1.0 + h, h)
    
    x_all = np.append([args[0]], x)
    x_all = np.append(x_all, [args[1]])
    
    fig, ax = pyplot.subplots()
    
    #pyplot.title("Bratu 1-D (Hybrid Powell method)")
    
    # Set general Axis format
    ax.grid(b=True, ls="--")
    pyplot.xlim(0.0, 1.0)
    #pyplot.ylim(0.0, 1.4)
    
    # Set X-axis format
    #ax.xaxis.set_major_formatter(pyplot.FuncFormatter(bratu_tick_format))
    ax.set_xlabel("time (s)")
    
    # Set Y-axis format
    # ???
    
    # PLOT
    pyplot.plot(t_series, x_all, '-', linewidth=2.0)

def plot_bratu2(x, labels, args):
    h = args[3]
    #steps = len(x) + 2
    t_series = np.arange(0.0, 1.0 + h, h)
    
    #print(t_series)
    
    x_all = [0, 0]
    
    x_all[0] = np.append([args[0]], x[0])
    x_all[0] = np.append(x_all[0], [args[1]])
    
    x_all[1] = np.append([args[0]], x[1])
    x_all[1] = np.append(x_all[1], [args[1]])
    
    fig, ax = pyplot.subplots()
    
    #pyplot.title("Bratu 1-D (Hybrid Powell method)")
    
    # Set general Axis format
    ax.grid(b=True, ls="--")
    pyplot.xlim(0.0, 1.0)
    #pyplot.ylim(0.0, 4.2)
    
    # Set X-axis format
    #ax.xaxis.set_major_formatter(pyplot.FuncFormatter(bratu_tick_format))
    ax.set_xlabel("time (s)")
    
    # Set Y-axis format
    # ???
    
    # PLOT
    pyplot.plot(t_series, x_all[0], '-', linewidth=2.0, label=labels[0])
    pyplot.plot(t_series, x_all[1], '--', linewidth=2.0, label=labels[1])
    pyplot.legend()
    
    # TODO configure
    

'''
x0 = initial value length of vector = # of time slices
t0 = initial time
args:
    [0] = u(0)
    [1] = u(1)
    [2] = r
    [3] = h
'''
def solve_bratu(x0, args):
    sol = opt.root(f, x0, args=args, jac=jacobian, method='hybr')
    #print(sol)
    return sol.x
    
def configure_pyplot():
    styleFile = os.path.join("..", "pyplot", "thesis_style")
    pyplot.style.use(styleFile)

def main():
    configure_pyplot()
    h = 0.02
    itns = int(1.0/h) - 1
    
    r = 3.4
    x0 = np.ones(itns)
    
    # Setup jacobian
    global jacobian_m 
    jacobian_m = np.zeros((itns,itns))
    for i in range(itns-1):
        jacobian_m[i, i+1] = jacobian_m[i+1, i] = 1.0 / (h**2)
    
    args = np.array([0.0, 0.0, r, h])
    sol = solve_bratu(x0, args)
    plot_bratu(sol, args)

def main2():
    configure_pyplot()
    h = 0.02
    itns = int(1.0/h) - 1
    
    x_sols = [0, 0]
    
    # First problem
    x0 = np.ones(itns)
    r = 3.4
    # Setup jacobian
    global jacobian_m 
    jacobian_m = np.zeros((itns,itns))
    for i in range(itns-1):
        jacobian_m[i, i+1] = jacobian_m[i+1, i] = 1.0 / (h**2)
    
    args = np.array([0.0, 0.0, r, h])
    x_sols[0] = solve_bratu(x0, args)
    # Plot first problem
    #plot_bratu(x_sols[0], args)
    
    # Second problem
    x0 = np.ones(itns) + 3.0
    r = 3.4
    # Setup jacobian
    # global jacobian_m 
    jacobian_m = np.zeros((itns,itns))
    for i in range(itns-1):
        jacobian_m[i, i+1] = jacobian_m[i+1, i] = 1.0 / (h**2)
    
    args = np.array([0.0, 0.0, r, h])
    x_sols[1] = solve_bratu(x0, args)
    # Plot second problem
    #plot_bratu(x_sols[1], args)
    
    lbls = ["Init val = 1.0", "Init val = 4.0"]
    plot_bratu2(x_sols, lbls, args)

if __name__ == "__main__":
    main2()