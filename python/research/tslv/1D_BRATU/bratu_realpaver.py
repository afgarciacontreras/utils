# -*- coding: utf-8 -*-
"""
@author: Angel Garcia
"""

import os
import subprocess
import re
import numpy as np
import matplotlib.pyplot as pyplot
import matplotlib.colors as mcolor

def runFile(inFile, outFile):
    realpaver_exe = os.path.join("..", "bin", "realpaver.exe")
    with open(outFile, "w") as f:
        subprocess.call([realpaver_exe, inFile], stdout=f)

"""
Create system of constraints + params
args:
    [0] = u(0)
    [1] = u(1)
    [2] = r
    [3] = h
"""
def createFile(filename, args, steps, zero_range=False):
    h = args[3]
    epsilon = "1.0e-8"
    contents = ""
    
    # HEADER
    #contents += ("OUTPUT\n" +
    #             "mode   = hull;\n\n")
    contents += ("BRANCH\n" +
                 "precision = " + epsilon + ",\n" +
                 "parts = 2,\n" +
                 "mode = paving,\n" +
                 "number = +oo;\n\n")
    # CONSTANTS
    contents += ("CONSTANTS\n" +
                 "h = {0:.4f},\n".format(h) +
                 "r = {0},\n".format(args[2]) + 
                 "x0 = {0:.4f},\n".format(args[0]) +
                 "x{0:d} = {1:.4f},\n".format(steps, args[1]) +
                 "z = " + ("[-" + epsilon + "," + epsilon + "];\n\n"
                           if zero_range else "0;\n\n"))
    # VARIABLES
    contents += "VARIABLES\n"
    for i in range(1, steps-1):
        contents += "x{0:d}\t in \t[0.0, 7.0],\n".format(i)
    contents += "x{0:d}\t in \t[0.0, 7.0];\n\n".format(steps-1)
    
    # CONSTRAINTS 
    contents += "CONSTRAINTS\n"
    # First equation
    #contents += "(-2*x0 + x1)/(h^2) + r * exp(x0) = z,\n"
    
    # From 1 to next-to-last equations
    base_eq = "(x{0:d} - 2*x{1:d} + x{2:d})/(h^2) + r * exp(x{1:d}) = z,\n"
    for i in range(1, steps-1):
        contents += base_eq.format(i-1, i, i+1)
    contents += base_eq.replace(",\n",";").format(steps-2, steps-1, steps)
    
    # Last equation
    #contents += "(x{0:d} -2*x{1:d})/(h^2) + r * exp(x{1:d}) = z;".format(
    #        steps-1, steps)
    
    with open(filename, "w") as f:
        f.write(contents)

def run_realpaver(inFile, outFile):
    realpaver_exe = os.path.join("..", "bin", "realpaver.exe")
    with open(outFile, "w") as f:
        subprocess.call([realpaver_exe, inFile], stdout=f)

def process_rp_output(outFile, args, steps):
    text = []
    isReliable = False
    # Get all the text data
    with open(outFile, "r") as f:
        inResults = False
        for line in f:
            if "OUTER BOX" in line or "HULL" in line:
                inResults = True
            elif "END OF SOLVING" in line:
                inResults = False
            if inResults:
                text.append(line)
            if "no solution is lost" in line:
                isReliable = True
    boxes = []
    curr_box = None
    # Process the text data
    patt = re.compile("^\s*x(\d+)\s*in\s*\[\s*(.+)\s*,\s*(.+)\s*\]\s*$")
    for line in text:
        if "OUTER BOX" in line:
            curr_box = np.zeros((steps + 1, 2),float)
        elif "precision" in line:
            if curr_box is not None:
                boxes.append(curr_box)
        else:
            m = patt.match(line)
            if m:
                i = int(m.group(1))
                lb = float(m.group(2))
                ub = float(m.group(3))
                curr_box[i,0] = lb
                curr_box[i,1] = ub
    #print(boxes)
    return (isReliable, boxes)

def plot_boxes(boxes, args, steps):
    t_series = np.arange(0.0, 1.0 + args[3], args[3])
    lastColor = 0.0
    colorStep = 1.0/len(boxes)
    fig, ax = pyplot.subplots()
    for box in boxes:
        #facecolor=mcolor.hsv_to_rgb((lastColor,0.9,0.9))
        ax.fill_between(t_series, box[:,0], box[:,1], 
                        color=mcolor.hsv_to_rgb((lastColor,0.9,0.75)),
                        linewidth=0.5, linestyle='-')
        lastColor += colorStep
    ax.grid(axis='x')
    pyplot.title("BRATU 1-D (RealPaver)")

"""
args:
    [0] = u(0)
    [1] = u(1)
    [2] = r
    [3] = h
"""
def main():
    steps = 100
    args = [0.0, 0.0, "[0.95,1.05]", 1.0/steps]
    
    inFilename = "bratu_rp4.txt"
    outFilename = "bratu_out4.txt"
    createFile(inFilename, args, steps, False)
    run_realpaver(inFilename, outFilename)
    isReliable, boxes = process_rp_output(outFilename, args, steps)
    
    import winsound
    frequency = 2500
    duration = 1000
    winsound.Beep(frequency, duration)
    
    if len(boxes) == 0:
        print("NO SOLUTION FOUND")
    else:
        print(len(boxes))
        plot_boxes(boxes, args, steps)

if __name__ == "__main__":
    main()