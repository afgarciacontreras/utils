# -*- coding: utf-8 -*-
"""
@author: Angel Garcia
"""

import os
import subprocess
import re
import numpy as np
import matplotlib.pyplot as pyplot
import matplotlib.colors as mcolor

def runFile(inFile, outFile):
    realpaver_exe = os.path.join("..", "bin", "realpaver.exe")
    with open(outFile, "w") as f:
        subprocess.call([realpaver_exe, inFile], stdout=f)

"""
Create system of constraints + params
args:
    x0 = [x, y, z]
    p = [a, b, c, d, e, f, g]
    h
"""
def createFile(filename, args, steps, zero_range=False):
    h = args[2]
    epsilon = "1.0e-8"
    contents = ""
    
    contents += "TIME = 1800000;\n\n"
    
    # HEADER
    #contents += ("OUTPUT\n" +
    #             "mode   = hull;\n\n")
    contents += ("BRANCH\n" +
                 "precision = " + epsilon + ",\n" +
                 "parts = 2,\n" +
                 "mode = paving,\n" +
                 "number = +oo;\n\n")
    # CONSTANTS
    contents += ("CONSTANTS\n" +
                 "h = {0:.4f},\n".format(h) +
                 "x0 = {0:.4f},\n".format(args[0][0]) +
                 "y0 = {0:.4f},\n".format(args[0][1]) +
                 "z0 = {0:.4f},\n".format(args[0][2]) +
                 "a = {0:.4f},\n".format(args[1][0]) +
                 "b = {0:.4f},\n".format(args[1][1]) +
                 "c = {0:.4f},\n".format(args[1][2]) +
                 "d = {0:.4f},\n".format(args[1][3]) +
                 "e = {0:.4f},\n".format(args[1][4]) +
                 "f = {0:.4f},\n".format(args[1][5]) +
                 "g = {0:.4f},\n".format(args[1][6]) +
                 "z = " + ("[-" + epsilon + "," + epsilon + "];\n\n"
                           if zero_range else "0;\n\n"))
    # VARIABLES
    contents += "VARIABLES\n"
    for i in range(1, steps):
        contents += "x{0:d}\t in \t[0.0, 300.0],\n".format(i)
        contents += "y{0:d}\t in \t[0.0, 300.0],\n".format(i)
        contents += "z{0:d}\t in \t[0.0, 300.0],\n".format(i)
    contents += "x{0:d}\t in \t[0.0, 300.0],\n".format(steps)
    contents += "y{0:d}\t in \t[0.0, 300.0],\n".format(steps)
    contents += "z{0:d}\t in \t[0.0, 300.0];\n\n".format(steps)
    
    # CONSTRAINTS 
    contents += "CONSTRAINTS\n"
        
    # Equations
    # Last equation
    bwd_eq = ""
    bwd_eq += "(x{0:d} - x{1:d}) / h - x{0:d} * (a - b * y{0:d}) = z,\n"
    bwd_eq += "(y{0:d} - y{1:d}) / h - y{0:d} * (-c + d * x{0:d} - e * z{0:d}) = z,\n"
    bwd_eq += "(z{0:d} - z{1:d}) / h - z{0:d} * (-f + g * y{0:d}) = z;\n\n"
    
    base_eq = ""
    base_eq += "(x{0:d} - x{2:d}) / 2 * h - x{1:d} * (a - b * y{1:d}) = z,\n"
    base_eq += "(y{0:d} - y{2:d}) / 2 * h - y{1:d} * (-c + d * x{1:d} - e * z{1:d}) = z,\n"
    base_eq += "(z{0:d} - z{2:d}) / 2 * h - z{1:d} * (-f + g * y{1:d}) = z,\n"
    for i in range(1, steps):
        contents += base_eq.format(i+1, i, i-1)
    contents += bwd_eq.format(steps, steps-1)
    
    
    with open(filename, "w") as f:
        f.write(contents)

def run_realpaver(inFile, outFile):
    realpaver_exe = os.path.join("..", "bin", "realpaver.exe")
    with open(outFile, "w") as f:
        subprocess.call([realpaver_exe, inFile], stdout=f)

def process_rp_output(outFile, args, steps):
    text = []
    isReliable = False
    # Get all the text data
    with open(outFile, "r") as f:
        inResults = False
        for line in f:
            if "OUTER BOX" in line or "HULL" in line:
                inResults = True
            elif "END OF SOLVING" in line:
                inResults = False
            if inResults:
                text.append(line)
            if "no solution is lost" in line:
                isReliable = True
    boxes = []
    curr_box = None
    # Process the text data
    patt = re.compile("^\s*x(\d+)\s*in\s*\[\s*(.+)\s*,\s*(.+)\s*\]\s*$")
    for line in text:
        if "OUTER BOX" in line:
            curr_box = np.zeros((steps + 1, 2),float)
        elif "precision" in line:
            if curr_box is not None:
                boxes.append(curr_box)
        else:
            m = patt.match(line)
            if m:
                i = int(m.group(1))
                lb = float(m.group(2))
                ub = float(m.group(3))
                curr_box[i,0] = lb
                curr_box[i,1] = ub
    #print(boxes)
    return (isReliable, boxes)

def plot_boxes(boxes, args, steps):
    t_series = np.arange(0.0, 1.0 + args[3], args[3])
    lastColor = 0.0
    colorStep = 1.0/len(boxes)
    fig, ax = pyplot.subplots()
    for box in boxes:
        #facecolor=mcolor.hsv_to_rgb((lastColor,0.9,0.9))
        ax.fill_between(t_series, box[:,0], box[:,1], 
                        color=mcolor.hsv_to_rgb((lastColor,0.9,0.75)),
                        linewidth=0.5, linestyle='-')
        lastColor += colorStep
    ax.grid(axis='x')
    pyplot.title("3SLV (RealPaver)")

"""
args:
    x0 = [x, y, z]
    p = [a, b, c, d, e, f, g]
    h
"""
def main():
    t0 = 0
    tf = 16.0
    steps = 64
    h = (tf-t0) / steps
    args = [[0.5, 0.5, 2.0], 
            [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.88], 
            h]
    
    inFilename = "3slv_rp.txt"
    outFilename = "3slv_out.txt"
    createFile(inFilename, args, steps, False)
    run_realpaver(inFilename, outFilename)
    #isReliable, boxes = process_rp_output(outFilename, args, steps)
    
    import winsound
    frequency = 2500
    duration = 1000
    winsound.Beep(frequency, duration)
    
    if len(boxes) == 0:
        print("NO SOLUTION FOUND")
    else:
        print(len(boxes))
        plot_boxes(boxes, args, steps)

if __name__ == "__main__":
    main()