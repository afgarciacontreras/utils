# -*- coding: utf-8 -*-
"""
Created on Fri Jun 29 12:58:52 2018

@author: Angel Garcia
"""

import numpy as np
import matplotlib.pyplot as pyplot

# Discretization of Three Species Lotka-Volterra
def f(p, x, h):
    x_next = np.array([0.0, 0.0, 0.0])
    # p[0] to p[6] = a, b, c, d, e, f, g
    # x[0] to x[2] =  x_n, y_n, z_n
    # x_next[0] to z_next[2] 
    
    #print(x)
    
    #x_next[0] = max(0.0, x[0] + h * (p[0] * x[0] - p[1] * x[0] * x[1]))
    #x_next[1] = max(0.0, x[1] + h * (-p[2] * x[1] + p[3] * x[0] * x[1] - p[4] * x[1] *x[2]))
    #x_next[2] = max(0.0, x[2] + h * (-p[5] * x[2] + p[6] * x[1] * x[2]))
    
    x_next[0] = x[0] + h * (p[0] * x[0] - p[1] * x[0] * x[1])
    x_next[1] = x[1] + h * (-p[2] * x[1] + p[3] * x[0] * x[1] - p[4] * x[1] *x[2])
    x_next[2] = x[2] + h * (-p[5] * x[2] + p[6] * x[1] * x[2])
    
    return x_next

def lv3s(p, x0, t0, tf, h, steps):
    x = np.zeros((steps+1, 3))
    x[0] = np.array(x0)
    for i in range(1,steps+1):
        x[i] = f(p, x[i-1], h)
    
    return x

def plot(x_series, t0, tf, h, title, filename, steps):
    x = x_series[:,0]
    y = x_series[:,1]
    z = x_series[:,2]
    
    t_series = (np.arange(steps+1) + t0) * h
        
    fig, ax = pyplot.subplots()
    
    pyplot.title(title)
    
    pyplot.plot(t_series, x, '-', linewidth=0.5, color=(0.95,0.0,0.0))
    pyplot.plot(t_series, y, '-', linewidth=0.5, color=(0.0,0.95,0.0))
    pyplot.plot(t_series, z, '-', linewidth=0.5, color=(0.0,0.0,0.95))
    
    #pyplot.savefig(filename, bbox_inches='tight',dpi=600)
    #pyplot.savefig(filename.replace(".eps",".png"), bbox_inches='tight', dpi=600)
    #pyplot.close()

def main():
    '''
    Example 1. Initial conditions $x(0) = .5, y(0) = .5, z(0) = 2$, parameters
    $a = b = c = d = e = f = 1$ and $g = 0.88$, with $t = [0 .. 70]$ 
    and a time step $h = 0.5$:
    '''
    x0 = [0.5, 0.5, 2.0]
    p = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.88]
    t0 = 0.0
    tf = 70.0
    steps = 500000
    h = (tf-t0) / steps
    
    
    x_series = lv3s(p, x0, t0, tf, h, steps)
    plot(x_series, t0, tf, h, "Three species Lotka-Volterra, Example 1", 
         "3slv_ex1.eps", steps)
    
    '''
    Example 2. Initial conditions $x(0) = .5, y(0) = 1, z(0) = 2$, parameters
    $a = b = c = d = e = f = 1$ and $g = 1.6$, with $t = [0 .. 16]$ 
    and a time step $h = 0.5$:
    '''
    x0 = [0.5, 1.0, 2.0]
    p = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.6]
    t0 = 0.0
    tf = 16.0
    steps = 40000
    h = (tf-t0) / steps
    
    #x_series = lv3s(p, x0, t0, tf, h, steps)
    #plot(x_series, t0, tf, h, "Three species Lotka-Volterra, Example 2", 
    #     "3slv_ex2.eps", steps)
   
if __name__ == "__main__":
    main()