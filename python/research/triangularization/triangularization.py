# -*- coding: utf-8 -*-
"""
Created on Wed May 03 13:10:44 2017

@author: Angel Garcia
"""
'''
triangularization.py

Generates a triangular mesh to use in Finite Element Method.
'''
def midpoint(p1, p2):
    x = (p1[0]+p2[0])/2.0
    y = (p1[1]+p2[1])/2.0
    return (x, y)

def refine_triangle(triangle):
    (p1, p2, p3) = triangle
    p12 = midpoint(p1, p2)
    p23 = midpoint(p2, p3)
    p13 = midpoint(p1, p3)
    new_t = [(p1, p12, p13),
             (p12, p2, p23),
             (p13, p23, p3),
             (p12, p23, p13)]
    return new_t

def refine_set(triangle_list):
    new_list = list()
    for t in triangle_list:
        new_ts = refine_triangle(t)
        new_list.extend(new_ts)
    return new_list

import matplotlib.lines as plines
import matplotlib.pyplot as pplot

# http://stackoverflow.com/questions/36470343/how-to-draw-a-line-with-matplotlib
def draw_line(p1, p2):
#    ax = pplot.gca()
#    xmin, xmax = ax.get_xbound()
#
#    if(p2[0] == p1[0]):
#        xmin = xmax = p1[0]
#        ymin, ymax = ax.get_ybound()
#    else:
#        ymax = p1[1]+(p2[1]-p1[1])/(p2[0]-p1[0])*(xmax-p1[0])
#        ymin = p1[1]+(p2[1]-p1[1])/(p2[0]-p1[0])*(xmin-p1[0])
#
#    l = plines.Line2D([xmin,xmax], [ymin,ymax])
#    ax.add_line(l)
#    return l
    x = [p1[0], p2[0]]
    y = [p1[1], p2[1]]
    pplot.plot(x,y)

def draw_triangle(t):
    p1,p2,p3 = t
    draw_line(p1, p2)
    draw_line(p2, p3)
    draw_line(p1, p3)

if __name__ == "__main__":
    initial_ts = [[(0.0, 0.0), (1.0, 0.0), (0.5, 0.5)],
                  [(1.0, 0.0), (1.0, 1.0), (0.5, 0.5)],
                  [(1.0, 1.0), (0.0, 1.0), (0.5, 0.5)],
                  [(0.0, 1.0), (0.0, 0.0), (0.5, 0.5)]]
    print "INITIAL TRIANGLES"
    for t in initial_ts:
        print t
    
    new_ts = refine_set(initial_ts)
    print "REFINED TRIANGLES"
    for t in new_ts:
        print t
    new_ts2 = refine_set(new_ts)
    print "REFINED TRIANGLES 2"
    for t in new_ts2:
        print t
    
    
    pplot.axis([0.0, 1.0, 0.0, 1.0])
    ax = pplot.gca()
    ax.set_autoscale_on(False)
    
    for t in new_ts2:
        draw_triangle(t)
    
    pplot.show()
    
#    print "REFINED TRIANGLES 2"
#    new_ts2 = refine_set(new_ts)
#    for t in new_ts2:
#        print t
    