# -*- coding: utf-8 -*-
"""
Created on Thu May 04 10:40:04 2017

@author: Angel Garcia
"""

'''
triangularization2.py

Generates a triangular mesh to use in Finite Element Method.
'''
import matplotlib.pyplot as pplot
import numpy as np

def midpoint(p1, p2):
    x = (p1[0]+p2[0])/2.0
    y = (p1[1]+p2[1])/2.0
    return (x, y)

def midpoint_type(p1, p2):
    if p1 == 0 or p2 == 0:
        return 0
    elif p1 == 2 or p2 == 2:
        return 2
    else:
        return 1

def refine_triangle(triangle, points, gcb):
    (i1, i2, i3) = triangle
    
    # Get Coordinate Points
    p1 = points[i1]
    p2 = points[i2]
    p3 = points[i3]
    
    # Get gcb
    t1 = gcb[i1]
    t2 = gcb[i2]
    t3 = gcb[i3]
    
    # Get Midpoints
    p12 = midpoint(p1, p2)
    p23 = midpoint(p2, p3)
    p13 = midpoint(p1, p3)
    
    # Get points from list
    # or add them, if not on list
    if p12 in points:
        i12 = points.index(p12)
    else:
        i12 = len(points)
        points.append(p12)
        gcb.append(midpoint_type(t1, t2))
    
    if p23 in points:
        i23 = points.index(p23)
    else:
        i23 = len(points)
        points.append(p23)
        gcb.append(midpoint_type(t2, t3))
    
    if p13 in points:
        i13 = points.index(p13)
    else:
        i13 = len(points)
        points.append(p13)
        gcb.append(midpoint_type(t1, t3))
    
    # Create new Triangles
    new_t = [(i1, i12, i13),
             (i12, i2, i23),
             (i13, i23, i3),
             (i12, i23, i13)]
    
    return new_t

def draw_line(p1, p2):
    x = [p1[0], p2[0]]
    y = [p1[1], p2[1]]
    pplot.plot(x,y)

def draw_triangle(triangle, points):
    (i1, i2, i3) = triangle
    
    # Get Coordinate Points
    p1 = points[i1]
    p2 = points[i2]
    p3 = points[i3]
    
    # Draw each line
    draw_line(p1, p2)
    draw_line(p2, p3)
    draw_line(p1, p3)

def refine_set(triangle_list, points, gcb):
    new_list = list()
    for t in triangle_list:
        new_ts = refine_triangle(t, points, gcb)
        new_list.extend(new_ts)
    return new_list

def area_triangle(triangle, points):
    (i1, i2, i3) = triangle
    
    # Get Coordinate Points
    p1 = points[i1]
    p2 = points[i2]
    p3 = points[i3]
    
    m = np.array([[1.0, p1[0], p1[1]],
                  [1.0, p2[0], p2[1]],
                  [1.0, p3[0], p3[1]]])
    
    area = np.linalg.det(m)
    
    return area

def all_areas(triangles, points):
    areas = []
    for t in triangles:
        a = area_triangle(t, points)
        areas.append(a)
    return areas

# Element Diffusion Matrix for P1 element
def locA(triangle, points):
    (i1, i2, i3) = triangle
    
    # Get Coordinate Points
    (x1, y1) = points[i1]
    (x2, y2) = points[i2]
    (x3, y3) = points[i3]
    
    dx23 = x2 - x3
    dx31 = x3 - x1
    dx12 = x1 - x2
    dy23 = y2 - y3
    dy31 = y3 - y1
    dy12 = y1 - y2
    
    A = 0.5*(dx31 * dy12 - dy31 * dx12) #Triangle area
    
    elmA = [[0 for i in range(3)] for j in range(3)]
    
    elmA[0][0] = 0.25 * (dx23 * dx23 + dy23 * dy23) / A
    elmA[0][1] = 0.25 * (dx23 * dx31 + dy23 * dy31) / A
    elmA[0][2] = 0.25 * (dx23 * dx12 + dy23 * dy12) / A
    
    elmA[1][0] = 0.25 * (dx31 * dx23 + dy31 * dy23) / A
    elmA[1][1] = 0.25 * (dx31 * dx31 + dy31 * dy31) / A
    elmA[1][2] = 0.25 * (dx31 * dx12 + dy31 * dy12) / A
    
    elmA[2][0] = 0.25 * (dx12 * dx23 + dy12 * dy23) / A
    elmA[2][1] = 0.25 * (dx12 * dx31 + dy12 * dy31) / A
    elmA[2][2] = 0.25 * (dx12 * dx12 + dy12 * dy12) / A

    return elmA

# Element Matrix from Convection Term
def locB(triangle, points, vx, vy):
    (i1, i2, i3) = triangle
    
    # Get Coordinate Points
    (x1, y1) = points[i1]
    (x2, y2) = points[i2]
    (x3, y3) = points[i3]
    
    dx32 = x3 - x2
    dx13 = x1 - x3
    dx21 = x2 - x1
    dy32 = y3 - y2
    dy13 = y1 - y3
    dy21 = y2 - y1
    
    elmB = [[0 for i in range(3)] for j in range(3)]
    
    elmB[0][0] = (-vx * dy32 + vy * dx32) / 6.0
    elmB[0][1] = (-vx * dy13 + vy * dx13) / 6.0
    elmB[0][2] = (-vx * dy21 + vy * dx21) / 6.0
    
    elmB[1][0] = elmB[0][0]
    elmB[1][1] = elmB[0][1]
    elmB[1][0] = elmB[0][2]
    
    elmB[2][0] = elmB[0][0]
    elmB[2][1] = elmB[0][1]
    elmB[2][0] = elmB[0][2]
        
    return elmB

# Element Mass Matrix for P1 element
def locM(triangle, points):
    (i1, i2, i3) = triangle
    
    # Get Coordinate Points
    (x1, y1) = points[i1]
    (x2, y2) = points[i2]
    (x3, y3) = points[i3]
    
    #dx23 = x2 - x3
    dx31 = x3 - x1
    dx12 = x1 - x2
    #dy23 = y2 - y3
    dy31 = y3 - y1
    dy12 = y1 - y2
    
    A = 0.5*(dx31 * dy12 - dy31 * dx12) #Triangle area
    c_diag = A / 6.0    #  diagonal constant
    c_off = A / 12.0    # off-diagonal constant
            
    elmM = [[c_diag if i==j else c_off
             for i in range(3)] for j in range(3)]

    return elmM

# Source function at the RHS of the equation
def SRC(x, y):
    return -2 * y**2 - 2 * x**2 + 2 * x * y**2 + 2 * y * x**2
 
if __name__ == "__main__":
    # Record of all points.
    # Updated when adding new points
    points = [(0.5, 0.5),
              (1.0, 0.0),
              (1.0, 1.0),
              (0.0, 1.0),
              (0.0, 0.0)]
    # Point type
    # 0 = Inner
    # 1 = Dirichlet
    # 2 = Newmann
    gcb = [0, 1, 2, 2, 1]
    # Record of Triangles
    # Contains indices of points
    triangles = [(0, 1, 2),
                 (0, 2, 3),
                 (0, 3, 4),
                 (0, 1, 4)]
    
    new_t = refine_set(triangles, points, gcb)
    area_t = all_areas(new_t, points)
    #new_t = refine_set(new_t, points, gcb)
    for t in new_t:
        draw_triangle(t, points)
    
    for i in range(len(gcb)):
        (x,y) = points[i]
        pt = gcb[i]
        pplot.text(x, y, str(pt))