# -*- coding: utf-8 -*-
"""
Created on Thu May 04 10:40:04 2017

@author: Angel Garcia
"""

'''
triangularization4.py

Generates a triangular mesh to use in Finite Element Method.
'''
import matplotlib.pyplot as pplot
import numpy as np

def midpoint(p1, p2):
    x = (p1[0]+p2[0])/2.0
    y = (p1[1]+p2[1])/2.0
    return (x, y)

def midpoint_type(p1, p2):
    if p1 == 0 or p2 == 0:
        return 0
    elif p1 == 2 or p2 == 2:
        return 2
    else:
        return 1

def refine_triangle(triangle, points, gbc):
    (i1, i2, i3) = triangle
    
    # Get Coordinate Points
    p1 = points[i1]
    p2 = points[i2]
    p3 = points[i3]
    
    # Get gbc
    t1 = gbc[i1][0]
    t2 = gbc[i2][0]
    t3 = gbc[i3][0]
    
    # Get Midpoints
    p12 = midpoint(p1, p2)
    p23 = midpoint(p2, p3)
    p13 = midpoint(p1, p3)
    
    # Get points from list
    # or add them, if not on list
    if p12 in points:
        i12 = points.index(p12)
    else:
        i12 = len(points)
        points.append(p12)
        gbc.append([midpoint_type(t1, t2), 0.0])
    
    if p23 in points:
        i23 = points.index(p23)
    else:
        i23 = len(points)
        points.append(p23)
        gbc.append([midpoint_type(t2, t3), 0.0])
    
    if p13 in points:
        i13 = points.index(p13)
    else:
        i13 = len(points)
        points.append(p13)
        gbc.append([midpoint_type(t1, t3), 0.0])
    
    # Create new Triangles
    new_t = [(i1, i12, i13),
             (i12, i2, i23),
             (i13, i23, i3),
             (i23, i13, i12)]
    
    return new_t

def draw_line(p1, p2):
    x = [p1[0], p2[0]]
    y = [p1[1], p2[1]]
    pplot.plot(x,y)

def draw_triangle(triangle, points):
    (i1, i2, i3) = triangle
    
    # Get Coordinate Points
    p1 = points[i1]
    p2 = points[i2]
    p3 = points[i3]
    
    # Draw each line
    draw_line(p1, p2)
    draw_line(p2, p3)
    draw_line(p1, p3)

def refine_set(triangle_list, points, gbc):
    new_list = list()
    for t in triangle_list:
        new_ts = refine_triangle(t, points, gbc)
        new_list.extend(new_ts)
    return new_list

def area_triangle(triangle, points):
    (i1, i2, i3) = triangle
    
    # Get Coordinate Points
    p1 = points[i1]
    p2 = points[i2]
    p3 = points[i3]
    
    m = np.array([[1.0, p1[0], p1[1]],
                  [1.0, p2[0], p2[1]],
                  [1.0, p3[0], p3[1]]])
    
    area = np.linalg.det(m)
    
    return area

def all_areas(triangles, points):
    areas = []
    for t in triangles:
        a = area_triangle(t, points)
        areas.append(a)
    return areas

# Element Diffusion Matrix for P1 element
def locA(triangle, points):
    (i1, i2, i3) = triangle
    
    # Get Coordinate Points
    (x1, y1) = points[i1]
    (x2, y2) = points[i2]
    (x3, y3) = points[i3]
    
    dx23 = x2 - x3
    dx31 = x3 - x1
    dx12 = x1 - x2
    dy23 = y2 - y3
    dy31 = y3 - y1
    dy12 = y1 - y2
    
    A = 0.5*(dx31 * dy12 - dy31 * dx12) #Triangle area
    
    elmA = [[0 for i in range(3)] for j in range(3)]
    
    elmA[0][0] = 0.25 * (dx23 * dx23 + dy23 * dy23) / A
    elmA[0][1] = 0.25 * (dx23 * dx31 + dy23 * dy31) / A
    elmA[0][2] = 0.25 * (dx23 * dx12 + dy23 * dy12) / A
    
    elmA[1][0] = 0.25 * (dx31 * dx23 + dy31 * dy23) / A
    elmA[1][1] = 0.25 * (dx31 * dx31 + dy31 * dy31) / A
    elmA[1][2] = 0.25 * (dx31 * dx12 + dy31 * dy12) / A
    
    elmA[2][0] = 0.25 * (dx12 * dx23 + dy12 * dy23) / A
    elmA[2][1] = 0.25 * (dx12 * dx31 + dy12 * dy31) / A
    elmA[2][2] = 0.25 * (dx12 * dx12 + dy12 * dy12) / A

    return elmA

# Element Matrix from Convection Term
def locB(triangle, points):
    (i1, i2, i3) = triangle
    
    # Get Coordinate Points
    (x1, y1) = points[i1]
    (x2, y2) = points[i2]
    (x3, y3) = points[i3]
    
    dx23 = x2 - x3
    dx31 = x3 - x1
    dx12 = x1 - x2
    dy23 = y2 - y3
    dy31 = y3 - y1
    dy12 = y1 - y2
    
    A = 0.5*(dx31 * dy12 - dy31 * dx12) #Triangle area
    
    def V1(x, y):
        return ((x2 * y3 - x3 * y2) +
                (y2 - y3) * x +
                (x3 - x2) * y)  / A
    def V2(x, y):
        return ((x3 * y1 - x1 * y3) +
                (y3 - y1) * x +
                (x1 - x3) * y)  / A
    def V3(x, y):
        return ((x1 * y2 - x2 * y1) +
                (y1 - y2) * x +
                (x2 - x1) * y)  / A

    rho = 1.0
    Cp = 50.0
    K = 2
    dt = 0.005
    
    F = rho * Cp / (K * dt)

    elmB = [[0 for i in range(3)] for j in range(3)]
    
    elmB[0][0] = F * A * (V1((x1+x2)/2, (y1+y2)/2)**2 +
                          V1((x1+x3)/2, (y1+y3)/2)**2) / 3
    elmB[0][1] = F * A * (V1((x1+x2)/2, (y1+y2)/2) * 
                          V2((x1+x2)/2, (y1+y2)/2)) / 3
    elmB[0][2] = F * A * (V1((x1+x3)/2, (y1+y3)/2) * 
                          V3((x1+x3)/2, (y1+y3)/2)) / 3
    elmB[1][0] = elmB[0][1]
    elmB[1][1] = F * A * (V2((x1+x2)/2, (y1+y2)/2)**2 +
                          V2((x2+x3)/2, (y2+y3)/2)**2) / 3 
    elmB[1][2] = F * A * (V2((x2+x3)/2, (y2+y3)/2) * 
                          V3((x2+x3)/2, (y2+y3)/2)) / 3
    elmB[2][0] = elmB[0][2]
    elmB[2][1] = elmB[1][2]
    elmB[2][2] = F * A * (V3((x3+x2)/2, (y3+y2)/2)**2 +
                          V3((x1+x3)/2, (y1+y3)/2)**2) / 3 
        
    return elmB

# Element Mass Matrix for P1 element
def locM(triangle, points):
    (i1, i2, i3) = triangle
    
    # Get Coordinate Points
    (x1, y1) = points[i1]
    (x2, y2) = points[i2]
    (x3, y3) = points[i3]
    
    #dx23 = x2 - x3
    dx31 = x3 - x1
    dx12 = x1 - x2
    #dy23 = y2 - y3
    dy31 = y3 - y1
    dy12 = y1 - y2
    
    A = 0.5*(dx31 * dy12 - dy31 * dx12) #Triangle area
    c_diag = A / 6.0    #  diagonal constant
    c_off = A / 12.0    # off-diagonal constant
            
    elmM = [[c_diag if i==j else c_off
             for i in range(3)] for j in range(3)]

    return elmM

# Source function at the RHS of the equation
def SRC(x, y):
    x1 = 20 * x - 10
    x2 = 20 * y - 10
    fmin = -2.062611051464291
    fmax = -0.506527392845854
    
    fact1 = np.sin(x1) * np.sin(x2);
    fact2 = np.exp(np.abs(100.0 - np.sqrt(x1**2 + x2**2)/np.pi))
    
    return (-0.0001* 200 * 2500 * (np.power((np.abs(fact1 * fact2) + 1), 0.1)
                                   - fmin) / (fmax - fmin)) / 2

def solve():
    # Record of all points.
    # Updated when adding new points
    points = [(0.5, 0.5),
              (1.0, 0.0),
              (1.0, 1.0),
              (0.0, 1.0),
              (0.0, 0.0)]
    # Point type
    # 0 = Inner
    # 1 = Dirichlet
    # 2 = Newmann
    gbc = [[0, 0.0], 
           [1, 0.0], 
           [1, 0.0], 
           [1, 0.0], 
           [1, 0.0]]
    # Record of Triangles
    # Contains indices of points
    triangles = [(0, 1, 2),
                 (0, 2, 3),
                 (0, 3, 4),
                 (0, 4, 1)]
    
    # Refine the mesh
    nref = 5
    for i in range(nref):
        triangles = refine_set(triangles, points, gbc)
    
    # Draw the mesh
    for t in triangles:
        draw_triangle(t, points)
    
    npt = len(points)
    ne = len(triangles)
            
    # Initialize those arrays
    Ag = np.zeros((npt, npt))
    b = np.zeros(npt)

    # Loop over the elements
    for l in range(ne):
        t = triangles[l]
        p1, p2, p3 = t
        x1, y1 = points[p1]
        x2, y2 = points[p2]
        x3, y3 = points[p3]
        
        # Compute local element matrices
        elmA = locA(t, points)
        elmB = locB(t, points)
        elmM = locM(t, points)
        
        for i in range(3):
            i1 = t[i]
            for j in range(3):
                j1 = t[j]
                # Assemble into the global coefficient matrix
                Ag[i1][j1] = Ag[i1][j1] + elmA[i][j] + elmB[i][j]
                # Form the RHS of the FEM equation
                x, y = points[j1]
                b[i1] = b[i1] + elmM[i][j] * SRC(x, y)
    
    # Impose the Dirichlet BC
    for m in range(npt):
        if gbc[m][0] == 1:
            for i in range(npt):
                b[i] = b[i] - Ag[i][m] * gbc[m][1]
                Ag[i][m] = 0
                Ag[m][i] = 0
            Ag[m][m] = 1.0
            b[m] = gbc[m][1]

    # Solve the linear system
    u_fem = np.linalg.solve(Ag, b)
    
    writeFileTriangles("triangles3.txt", triangles)
    writeFileTriangles("points3.txt", points)
    
    return u_fem

def writeFile(filename, vector):
    all_t = "\n".join([str(x) for x in vector])
    with open(filename, "w") as f:
        f.write(all_t)

def writeFileTriangles(filename, vector):
    all_t = "\n".join(" ".join([str(x) for x in row]) for row in vector)
    with open(filename, "w") as f:
        f.write(all_t)
        
if __name__ == "__main__":
    u_fem = solve()
    #all_t = "\n".join([" ".join([str(x) for x in row]) for row in u_fem])
    all_t = "\n".join([str(x) for x in u_fem])
    with open("results3.txt", "w") as f:
        f.write(all_t)