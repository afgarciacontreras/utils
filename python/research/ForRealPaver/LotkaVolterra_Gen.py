# -*- coding: utf-8 -*-
"""
Created on Thu Apr 20 12:41:38 2017

@author: Angel Garcia
"""

h = 0.1
n = 100

local = "hc3"

lines = []

lines.append("TIME = 240000;")
lines.append("")

lines.append("BRANCH")
lines.append("parts = 3,")
lines.append("mode = paving,")
lines.append("number = +oo;")
lines.append("")


lines.append("CONSTANTS")
lines.append("h = {0},".format(h))
lines.append("v0 = 1.2,")
lines.append("w0 = 1.1;")
lines.append("")

lines.append("VARIABLES")
for i in range(1,n):
    lines.append("v{0} in [0.5, 1.5],".format(i))
    lines.append("w{0} in [0.5, 1.5],".format(i))
lines.append("v{0} in [0.5, 1.5],".format(n))
lines.append("w{0} in [0.5, 1.5];".format(n))
lines.append("")

lines.append("CONSTRAINTS")
for i in range(1,n):
    lines.append("v{2} - v{0} - 6 * h * v{1} * (1 - w{1}) = 0,".format(i-1,i,i+1))
    lines.append("w{2} - w{0} - 2 * h * w{1} * (v{1} - 1) = 0,".format(i-1,i,i+1))
lines.append("v{1} - v{0} - 3 * h * v{1} * (1 - w{1}) = 0,".format(n-1,n))
lines.append("w{1} - w{0} - h * w{1} * (v{1} - 1) = 0;".format(n-1,n))

with open("lotka-volterra_3B.txt","w+") as f:
    f.write("\n".join(lines))