# -*- coding: utf-8 -*-
"""
Created on Thu Apr 20 12:41:38 2017

@author: Angel Garcia
"""

h = 0.1
n = 50

local = "hc3"

lines = []

lines.append("TIME = 240000;")
lines.append("")

lines.append("BRANCH")
lines.append("parts = 3,")
lines.append("mode = paving,")
lines.append("number = +oo;")
lines.append("")


lines.append("CONSTANTS")
lines.append("h = {0},".format(h))
lines.append("v0 = 1.25,")
lines.append("w0 = 1.012092,")
lines.append("theta1 = [2.95, 3.05],")
lines.append("theta2 = [0.95, 1.05];")
lines.append("")

lines.append("VARIABLES")
for i in range(1,n):
    lines.append("v{0} in [0.5, 1.5],".format(i))
    lines.append("w{0} in [0.5, 1.5],".format(i))
lines.append("v{0} in [0.5, 1.5],".format(n))
lines.append("w{0} in [0.5, 1.5];".format(n))
lines.append("")

lines.append("CONSTRAINTS")
for i in range(1,n):
    lines.append("v{1} - v{0} - h * theta1 * v{1} * (1 - w{1}) = 0,".format(i-1,i,i+1))
    lines.append("w{1} - w{0} - h * theta2 * w{1} * (v{1} - 1) = 0,".format(i-1,i,i+1))
lines.append("v{1} - v{0} - theta1 * h * v{1} * (1 - w{1}) = 0,".format(n-1,n))
lines.append("w{1} - w{0} - theta2 * h * w{1} * (v{1} - 1) = 0;".format(n-1,n))

with open("lotka-volterra3.txt","w+") as f:
    f.write("\n".join(lines))