# -*- coding: utf-8 -*-
"""
Created on Tue Apr 25 09:21:28 2017

@author: Angel Garcia
"""

def read_base(filename):
    base = []
    with open(filename, "r+") as f:
        for line in f:
            base.append(line.split())
    return base


def size_base(base):
    return (len(base), len(base[0]))


def create_replacement(variable, base_row):
    replacement = []
    for i,v in enumerate(base_row):
        replacement.append("{0}*p{1}".format(v,i+1))
    repl_str = " + ".join(replacement)
    return "(" + repl_str + ")"

def create_variables(cols, domain_range):
    lines = []
    for i in range(cols):
        lines.append("p{0} in [{1} , {2}]".format(i+1, domain_range[i][0],
                                                      domain_range[i][1]))
    return ",\n".join(lines) + ";"

def obtain_rom(input_name, base_name, rom_domain_range):
    new_lines = []
    base = read_base(base_name)
    (row, col) = size_base(base)
    replacements = {}
    with open(input_name, "r+") as f:
        in_constraints = False
        in_variables = False
        for line in f:
            if "variables" in line.lower():
                in_variables = True
                new_lines.append(line)
                continue
            if "constraints" in line.lower():
                in_constraints = True
                new_lines.append(line)
                continue
            if in_variables:
                # Read Each Variable,
                split_v = line.split()
                if len(split_v) == 0:
                    continue
                var = split_v[0]
                var_row = len(replacements)
                base_row = base[var_row]
                # Create replacement
                repl = create_replacement(var, base_row)
                replacements[var] = repl
                # If last variable:
                if ";" in line:                
                    in_variables = False
                    # add new variables
                    new_lines.append(create_variables(col, rom_domain_range))
                continue
            if in_constraints:
                # take constraint, 
                constr = line
                if line.strip() == "":
                    continue
                # For each var in replacements,
                x = replacements.keys()
                x.sort()
                x.reverse()
                for var in x:
                    # ...replace var for replacement
                    repl = replacements[var]
                    if "w50" in constr or "v50" in constr:
                        if var == "v5" or var == "w5":
                            continue
                    constr = constr.replace(var,repl)
                new_lines.append(constr)
                if ";" in line:                
                    in_constraints = False
                continue
            # Otherwise, copy the line as-s
            new_lines.append(line)
    final_str = "".join(new_lines)
    with open("ROM.txt", "w+") as f:
        f.write(final_str)
        
if __name__ == "__main__":
    filename = "lotka-volterra3re.txt"
    filename_base = "Phi2.txt"
    ranges = []
    for i in range(6):
        ranges.append((-20.0, 20.0))
    obtain_rom(filename, filename_base, ranges)