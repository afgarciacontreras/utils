# -*- coding: utf-8 -*-
"""
Created on 11/13/2017

@author: Angel Garcia
"""

'''
FHN_central_gen.py
Used to generate a file that can be consumed by RealPaver, an interval-based
Constraint Solver.

The file represents a Fitz-Hugh Nagumo model, discretized using finite differences
'''

import math
import subprocess

def get_equations_at(i):
	"""
	INITIAL VALUES
	Z[0] = 0 + interval(-0.005, 0.005);
    Z[1] = -0.06 + interval(0.0, 0.0);
	
	PARAMETERS
	vspode->rpar[0] = 0.139 ; = alpha
    vspode->rpar[1] = 0.008 ; = epsilon
    vspode->rpar[2] = 2.54 ; = gamma
	
	EQUATION
	yp[0] = y[0] * (y[0] - rpar[0]) * (1 - y[0]) - y[1];
    yp[1] = rpar[1] * (y[0] - rpar[2] * y[1]);
	
	SOLVING PROCESS
	double t0 = 0, tend = 50;
    double tstep = 1.0;
	"""
	k1v = "v[{0}] * (v[{0}] - alpha) * (1 - v[{0}]) - w[{0}]".format(i - 1)
	k1w = "epsilon * (v[{0}] - gamma * w[{0}])".format(i - 1)
	
	vi_ = "v[{0}] + h * ({1})".format(i-1, k1v)
	wi_ = "w[{0}] + h * ({1})".format(i-1, k1w)
	
	k2v = "({0}) * (({0}) - alpha) * (1 - ({0})) - ({1})".format(vi_, wi_)
	k2w = "epsilon * (({0}) - gamma * ({1}))".format(vi_, wi_)
	
	kv = "(({0}) + ({1}))/2".format(k1v, k2v)
	kw = "(({0}) + ({1}))/2".format(k1w, k2w)
	
	vi = "v[{0}] = v[{1}] + ({2}) * h".format(i, i-1, kv)
	wi = "w[{0}] = w[{1}] + ({2}) * h".format(i, i-1, kw)
	
	return (vi, wi)

def get_system(N):
	equations_list = list()
	for i in range(1,n+1):
		vi,wi = get_equations_at(i)
		equations_list.append(vi)
		equations_list.append(wi)
	return "CONSTRAINTS\n" + ",\n".join(equations_list) + ";\n"
	
def get_constants(alpha, epsilon, gamma, h):
	cons_str = "CONSTANTS\n" + \
			   "h = {0:.16f},\n".format(h) + \
	           "alpha = {0:.16f},\n".format(alpha) + \
			   "epsilon = {0:.16f},\n".format(epsilon) + \
		       "gamma = {0:.16f};\n".format(gamma)

def get_variables(v0, w0, N):
	vars_str = "VARIABLES\n" +
	           "v[0] = [{0:.16f}, {1:.16f}],\n".format(v0[0], v0[1]) + \
			   "w[0] = [{0:.16f}, {1:.16f}],\n".format(w0[0], w0[1]) + \
			   "v[1..{0}] = [-100, 100],\n".format(N) + \
			   "w[1..{0}] = [-100, 100];\n".format(N)
	return vars_str

def get_branch():
	return "BRANCH\n" + \
	       "parts = 3, mode = paving, number = +oo;\n"
	

if __name__ == "__main__":
    filename = "FHN_rp.txt"
	N = 50
	h = 1.0
	v0 = (0.0, 0.0)
	w0 = (-0.06, -0.06)
	alpha = 0.139
	epsilon = 0.008
	gamma = 2.54
	str_to_write = ""
	str_to_write = str_to_write + get_branch() + "\n"
	str_to_write = str_to_write + get_constants(alpha, epsilon, gamma, h) + "\n"
	str_to_write = str_to_write + get_variables(v0, w0, N) + "\n"
	str_to_write = str_to_write + get_system(N)
	with open(filename, "w+") as f:
		f.write(str_to_write)

