# -*- coding: utf-8 -*-
"""
Created on Mon Apr 24 10:01:40 2017

@author: Angel Garcia
"""

'''
HeatEquation.py
Used to generate a file that can be consumed by RealPaver, an interval-based
Constraint Solver.

The file represents a heat-transfer ODE problem, discretized using finite differences
'''

import math
import subprocess

def f_q(x,y):
    fmin = -2.062611051464291
    fmax = -0.506527392845854
    x_m = 20.0*x - 10
    y_m = 20.0*y - 10
    inner_3 = abs(100 - math.sqrt(x_m**2 + y_m**2)/math.pi)
    inner_2 = abs(math.sin(x_m) * math.sin(y_m) * math.exp(inner_3)) + 1
    inner_1 = -1.0e-4 * inner_2**0.1
    Q = (inner_1 - fmin) / (fmax-fmin)
#    if Q < 0:
#        Q = 0
#    if Q > 1:
#        Q = 1
    return Q*200*2500

def write_rp(v, N, M):
    h = 1.0/(N+1)
    k = 1.0/(M+1)
    lines = []
    
    ###########################################################
    
    lines.append("BRANCH")
    lines.append("parts = 3,")
    lines.append("mode = paving,")
    lines.append("number = +oo;")
    
    ###########################################################
    
    
    lines.append("CONSTANTS")
    
    constants = []
    
    constants.append("rho = 1.0")
    constants.append("cp = 50.0")
    constants.append("alpha = 0.01")
    constants.append("beta = 2.0")
    constants.append("dt = 0.005")
    constants.append("h = {0:.16f}".format(h))
    constants.append("k = {0:.16f}".format(k))
    constants.append("fmin = -2.062611051464291")
    constants.append("fmax = -0.506527392845854")
    
    for i in range(1,M+1):
        for j in range(1,N+1):
            v_var = "T_{0}_{1}".format(i,j)
            constants.append("T0_{0}_{1} = [{2},{3}]".format(i,j,
                             v[v_var][0], v[v_var][1]))
    
    for i in range(1,M+1):
        constants.append("T_{0}_0 = {1}".format(i,0))
        constants.append("T_{0}_{2} = {1}".format(i,0,M+1))
    
    for j in range(1,N+1):
        constants.append("T_0_{0} = {1}".format(j,0))
        constants.append("T_{2}_{0} = {1}".format(j,0,N+1))
    
    for i in range(1,M+1):
        for j in range(1,N+1):
            constants.append("Q_{0}_{1} = {2:.16f}".format(i,j,f_q(i*h,j*k)))
    
    consts_total = ",\n".join(constants) + ";\n"
    lines.append(consts_total)
    
    ###########################################################
    
    variables = []
    
    lines.append("VARIABLES")
    
    for i in range(1,M+1):
        for j in range(1,N+1):
            variables.append("T_{0}_{1} in [{2},{3}]".format(i,j,0,1000))
    
    vars_total = ",\n".join(variables) + ";\n"
    lines.append(vars_total)
    
    ###########################################################
    
    lines.append("CONSTRAINTS")
    eq = "rho * cp * ( ( T_{0}_{3} - T0_{0}_{3} ) / dt ) = " + \
         "alpha * ( ( (T_{2}_{3} - T_{1}_{3} ) / (2*h) )^2 + " + \
         "( (T_{0}_{5} - T_{0}_{4} ) / (2*k) )^2 ) + " + \
         "(beta + alpha * T_{0}_{3}) * " + \
         "( ( T_{1}_{3} - 2*T_{0}_{3} + T_{2}_{3} )/h^2 + "+ \
         "( T_{0}_{4} - 2*T_{0}_{3} + T_{0}_{5} )/k^2 )" + \
         "+ Q_{0}_{3}"
    
    constraints = []
    
    for i in range(1,M+1):
        for j in range(1,N+1):
            constraints.append(eq.format(i,i-1,i+1,j,j-1,j+1))
    
    constr_total = ",\n".join(constraints) + ";\n"
    lines.append(constr_total)
    
    ###########################################################
    
    with open("heat_equation.txt","w+") as f:
        f.write("\n".join(lines))

def init_vars(N, M):
    v = dict()
    for i in range(1,N+1):
        for j in range(1,M+1):
            var = "T_{0}_{1}".format(i,j)
            v[var] = ("0","0")
    return v

def extract_T(filename):
    v = dict()
    with open(filename, "r+") as f:
        in_hull = False
        for line in f:
            if "OUTER BOX" in line:
                in_hull = True
                continue
            if in_hull:
                if len(line.strip()) == 0:
                    break
                sp_line = line.split(" ")
                var = sp_line[2]
                ub = sp_line[-1].replace("]","")
                lb = sp_line[-3].replace("[","")
                v[var] = (lb,ub)
    return v



if __name__ == "__main__":
    N = 10
    M = 10
    v = init_vars(N, M)

    for i in range(50):
        # Write RP file
        write_rp(v, N, M)
        
        # Execute RP file
        fname = "heat{0}.out".format(i+1)
        outputFile =  open(fname, "w+")
        subprocess.call(['realpaver', '-hc4', '-hull', "heat_equation.txt"], 
                        stdout=outputFile)
        outputFile.close()
        
        # Process RP output -> v
        v = extract_T(fname)
    