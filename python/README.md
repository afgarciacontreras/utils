# PYTHON CODES

* _inclass_: Scripts used to automate some boring in-class stuff, like unzipping files, sending emails, and choosing ~~victims~~ participants for in-class activities.
* _presentation_: I did a presentation on Python for the local SIAM chapter. Here are the source files used as sample code for that presentation.
* _research_: Various scripts used in research. For the most part, they are file-generators. The generated files are consumed by other applications.