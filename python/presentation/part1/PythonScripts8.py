

# Outer function
def incArray(a):
	# Nested Function
	# Can be called only inside incArray
	def incNum(n):
		return n+1
	for i in range(len(a)):
		a[i] = incNum(a[i])

a = [1,2,3,4]
print(a)
incArray(a)
print(a)
# If you uncomment the next line,
# the program crashes
# print(incNum(a[0]))

# Functional Closure:
# This is a function
# that makes functions!
def incMaker(inc=1):
	def incFun(a):
		for i in range(len(a)):
			a[i] = a[i] + inc
	return incFun # nested function name
# Default value, inc by 1
f = incMaker() # f is a new function
f(a) #increase all elems by 1
print(a)
f = incMaker(3) # f is a new function
f(a) #increase all elems by 3
print(a)



x = list(range(6))
x2 = map(lambda n : n + 0.1, x)
x2 = list(x2)
print(x)
print(x2)

y = x[::-1]*2
y2 = filter(lambda n : n%2==1, y)
y2 = list(y2)
print(y)
print(y2)