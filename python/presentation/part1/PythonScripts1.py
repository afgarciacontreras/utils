
# To test: Change this value
x = 12
if x == 10: # You don't need parenthesis
	# Tabulation is what indicates
	print("The Value of X is 10")
elif (x < 10): # You can use parenthesis tho
	for i in range(x):
		# Must convert i to string
		print(str(i + 1))
	print("Tequila!")
else:
	i = x
	while i < 50000:
		print("Spam x " + str(i))
		i = (i / 2) ** 2 - 1
	print("Lovely Spam!")
	print("Wonderful Spam!")
