

# File to Read...
fileIn = open("file.txt", "r")
# File to Write...
fileOut = open("elif.txt", "w")
# Iterate through the lines.
for line in fileIn:
	print(line.strip())
	# Write each line, reversed
	fileOut.write(line[::-1])
# Must close files.
fileIn.close()
fileOut.close()
	
print("\n\n")

# File to read
with open("file.txt", "r") as fileIn:
	# File to write
	with open("elif.txt", "w") as fileOut:
		# Iterate through the lines.
		for line in fileIn:
			print(line.strip())
			# Write each line, reversed
			fileOut.write(line[::-1])
# No need to close files,
# the with command does it.