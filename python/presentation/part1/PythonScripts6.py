
# Creating new Dictionaries
a = dict()
b = {"one":3, "two":1, "three":2}
c = dict([("three",2), ("two",1), ("one",3)])
print(b)
print(b["one"])
print(c)
# Element order does not matter
print(b==c)
print(2 in b)
print("two" in b)
# keys vs values vs items
# (can convert to lists)
print(b.keys())
print(b.values())
print(b.items())
# Convert keys <-> values
zipped = zip(list(b.values()), list(b.keys()))
d = dict(list(zipped))
print(d)
# Dictionary Comprehensions
e = {n:("#"+str(2**n)) for n in range(8)}
print(e)

