
# Function definition
# This function reverses an int
def reverseInt(x):
	rev = 0
	while x > 0:
		rev = (rev * 10) + (x % 10)
		x = int(x // 10) # Integer division
	return rev

print(reverseInt(1234))
print(reverseInt(9432))
# In Python, Functions are Objects too!
print(reverseInt)

# Class Definition
class Shape():
	# Class variable
	shapes = 0
	# Constructor
	def __init__(self):
		# Instance variables
		self.sides = 0
		self.size = 0
		# Update class variable
		Shape.shapes = Shape.shapes + 1
	# Instance Methods
	def calculatePerimeter(self):
		return self.sides * self.size

# Inheritance
class Triangle(Shape):
	def __init__(self, size):
		super().__init__()
		self.sides = 3
		self.size = size

newShape = Shape()
print(newShape)
print(newShape.calculatePerimeter())
newShape = Triangle(12)
print(newShape)
print(newShape.calculatePerimeter())
print(Shape.shapes)
