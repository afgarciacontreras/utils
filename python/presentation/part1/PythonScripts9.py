
# First Version
# Iterate on range and print?
def spamEggs1(n):
	for i in range(1, n+1):
		out = ""
		if i % 3 == 0:
			out += "Spam"
		if i % 5 == 0:
			out += "Eggs"
		if out == "":
			print(str(i), end=", ")
		else:
			print(out, end=", ")

spamEggs1(25)
print("")

# Second Version
# Function that returns the string
# of a specific number
def spamOrEggs(x):
	out = ""
	if x % 3 == 0:
		out += "Spam"
	if x % 5 == 0:
		out += "Eggs"
	if out == "":
		return str(x)
	else:
		return out

# Method that prints all the list
def spamEggs2(n):
	print([spamOrEggs(i+1) for i in range(n)])

spamEggs2(25)

# Third Version
# A SpamEggs Generator!
def spamEggsGen(x):
	for i in range(x):
		out = ""
		if (i+1) % 3 == 0:
			out += "Spam"
		if (i+1) % 5 == 0:
			out += "Eggs"
		if out == "":
			# No return,
			# yield!
			yield str(i+1)
		else:
			# No return,
			# yield!
			yield out

# Method that iterates through
# the results of the generator			
def spamEggs3(n):
	for x in spamEggsGen(n):
		print(x, end=", ")

spamEggs3(25)

