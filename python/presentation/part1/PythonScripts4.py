
# these also work for strings
x = range(10)
print(5 in x)
print(100 in x)
print(100 not in x)
# addition = concatenation
y = list(x) + [0,5,3,8]
print(y)
# multiplication = multiple addition
z = [0,5,3,8]*3
print(z)
#Slicing: from beginning to 5
print(y[:5])
#Slicing: from 5 to end
print(y[5:])
#Slicing: just 5
print(y[5:6])
#Slicing: from beginning to 5, step 2
print(y[:5:2])
#Slicing: from 5 to end, step -1
print(y[5::3])
#Slicing: return reversed
print(y[::-1])

print("----------")

def inc(n):
	return n + 0.1
x = list(range(6))
x2 = map(inc, x)
x2 = list(x2)
print(x)
print(x2)

def is2plus1(n):
	return n%2 == 1
y = x[::-1]*2
y2 = filter(is2plus1, y)
y2 = list(y2)
print(y)
print(y2)

z = zip(x2, y2)
print(list(z))
x3, y3 = zip(*zip(x2, y2))
print(x3)
print(y3)

