# Anything can go in a list
a = list()
b = [1, 2.0, "3.0", a]

print(a)
print(b)
print(b[0])
print("--")
for x in b:
	print(x)
print("--")
a.append("Yes!")
# This will also print a modified "a"
# "b" has a reference to "a"
for x in b:
	print(x)
print("--")
# puts "a" after "b"
b.extend(a)
for x in b:
	print(x)
print("--")
print(min([1,2,3,4,50,0,-3,10]))
print(max([1,2,3,4,50,0,-3,10]))
print(sum([1,2,3,4,50,0,-3,10]))

# List Comprehensions
print([2**x for x in range(8)])
print([x % 3 == 0 for x in range(1,9)])
# Comprehensions with Filtering 
y = [x for x in range(16) if x%3==0 or x%5==0]
print(y)

z = list(range(10))
del z[5]
print(z)
z.reverse()
print(z)
z[5] = -1
z.sort()
print(z)