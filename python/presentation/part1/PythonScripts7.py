
# Default parameters
def incDec(n, inc=True):
	#condition expression
	return n+1 if inc else n-1

# print 6, 6, 4
print(incDec(5))
print(incDec(5,True))
print(incDec(5,False))

# Variable number of parameters
def printIf(keyword, *args):
	for a in args:
		if keyword in a:
			print(a, end=",")
	print("")

printIf("a")
printIf("a", "banana") 
printIf("a", "banana", "angel") 
printIf("e", "green", "red", "blue")
printIf("d", "green", "red", "blue")

# This variable will also be in
# incElems's scope
incVar = 2
# Method modifies arrays.
# (Arrays are mutable)
def incElems(a):
	for i in range(len(a)):
		a[i] = a[i] + incVar

x = [1, 2, 3, 4]
print(x)
incElems(x)
print(x)
# Changing incVar also changes
# what happens in incElems
incVar = 1 
incElems(x)
print(x)