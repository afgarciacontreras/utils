
# Anything can go in a tuple
a = tuple()
b = (1, 2.0, "3.0", a)
c = (4,) #This creates a size 1 tuple
print(a)
print(b)
print(c)
print(b[0])

# These two cause errors
# To test, uncomment individually
#c[0] = "yes"
#del b[1]

# Most list operations work...
# as long as they do not modify
# the tuple
print(len(b))
print(c + b)
print(c * 4)

s = (1, 2, 3)
x, y, z = s
print(x)
print(y)
print(z)

def around(n):
	return (n-1, n, n+1)

w, t, f = around(10)
print(w)
print(t)
print(f)