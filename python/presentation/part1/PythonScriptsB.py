

# From Standard Library
import sys
# Number of arguments
argNum = len(sys.argv)
print(argNum)
# argv[0] is the filename
print(sys.argv)
# If the user provided an argument,
# use it to determine the # of inputs
n = 5 if argNum == 1 else int(sys.argv[1])
arr = [0]*n
# Fill an array with user input
for i in range(n):
	# str.format() to format numeri inputs
	arr[i] = input("Input #{0}: ".format(i+1))
print(arr)

