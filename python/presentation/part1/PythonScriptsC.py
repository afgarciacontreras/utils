

import os
import shutil
import glob

# Print all files in current dir
print(os.listdir("."))
# Print the absolute path
print(os.path.abspath("."))

shutil.copyfile("file.txt", "copy.txt")


print("file.txt has been copied.")
input("Press Enter to continue.")

os.remove("copy.txt")

print("copy.txt has been removed.")
input("Press Enter to continue.")

print(glob.glob("*.txt"))
input("Press Enter to continue.")

print(glob.glob("*.py"))
input("Press Enter to continue.")