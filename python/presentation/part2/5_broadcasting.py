# -*- coding: utf-8 -*-
import numpy as np

# ARRAY BROADCASTING
# Normal operation...
x = np.arange(1,10).reshape((3,3))
# Same size as x
y = np.zeros_like(x)
# Loop!
for row in y:
    row[1] = 1
print(x)
print(y)
print(x * y)

# ARRAY BROADCASTING
# Normal operation...
x = np.arange(1,10).reshape((3,3))
# Same size as x
y = np.zeros_like(x)
# No Loop!
# Broadcast the 1 across all rows...
y[:,1] = 1 # this 1 is a size-1 array
print(x)
print(y)
print(x * y)

# ARRAY BROADCASTING
# Normal operation...
x = np.arange(1,10).reshape((3,3))
# Same size as a single row of x
y = np.array([0,1,0])
print(x)
print(y)
# No loop!
# y is broadcasted across all rows
print(x * y)

