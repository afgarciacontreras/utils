# -*- coding: utf-8 -*-
import numpy as np

# Create a new ndarray...
# 1. Using existing list.
x = np.array([1, 2, 3, 4, 5])
print(x)
x = np.array([1.0, 2.0, 3.0, 4.0, 5.0])
print(x)
# ... Specifying the type of the array
x = np.array([1, 2, 3, 4, 5], dtype=complex)
print(x)

# 2. Using a string
x = np.fromstring('1, 2, 3, 4', sep=',')
print(x)
x = np.fromstring('4 5 6 7', sep=' ', dtype=int)
print(x)

# 3. Using special methods
# ... Full of zeroes
print(np.zeros((5))) # One dimension
print(np.zeros((4,3))) # Two dimensions
print(np.zeros((2,2,2))) # Three dimensions

print(np.ones((2,3)))  # Full of ones
print(np.full((3,2), 5)) # Full of a single number

print(np.eye(4,4)) # Identity (diagonal is ones)
print(np.random.random((3,3))) # Random numbers between 0 and 1

# 1D Arrays (Vector) generators
# arange (between two numbers, w/ step size)
print(np.arange(5))
print(np.arange(4,10))
print(np.arange(10,20,2))
# linspace (between two numbers, w/ # of samples)
print(np.linspace(1,5)) # Default: 50 samples
print(np.linspace(1,5,10))
print(np.linspace(1,2,10,retstep=True))
# logspace (evenly, on a log scale, default base = 10.0)
print(np.logspace(1,5)) # Default: 50 samples
print(np.logspace(1,5,10))
print(np.logspace(1,5,10,base=2.0))
print(np.logspace(1,5,10,base=0.5))

