# -*- coding: utf-8 -*-
import numpy as np

# RESHAPE - a different view
x = np.arange(24)
y = x.reshape((4,6))
print(x)
print(y)
y[0,0] = 20 #Changes view...
print(x) #.. also changes original
# Reshape size must be compatible
# (Uncomment next line for error)
# y = x.reshape((10,10))

#RESIZE - actual change
# New size does not need to be compatible
y = np.resize(x, (2,3))
print(y)
# If size is larger, fill with copies
# (Copied data is linear, though)
z = np.resize(y, (6,3))
print(z)
z = np.resize(y, (6,6))
print(z)
z = np.resize(y, (6,9))
print(z)

# Basic Array Arithmetics
x = np.arange(9).reshape((3,3))
print(x)
print(x + 1)
print(x * 3)
print(x / 2)
y = np.eye(3,3)
print(x + y)
print(x * y)
print((y * 10) / (x + 1))

# Array Arithmetics & Functions
x = np.arange(4) + 1
y = np.linspace(25,100,4)
print(x)
print(y)
print(np.dot(x,y)) # Dot Product
# For 2D arrays: Matrix Multiplication
xM = x.reshape((2,2))
yM = y.reshape((2,2))
print(np.dot(xM, yM))
print(np.matmul(xM,yM))


