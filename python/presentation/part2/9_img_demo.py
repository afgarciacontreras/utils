# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage

# Prepare to show multiple images in PyPlot
imgList = list()


img_orig = plt.imread('horosho.jpg')
imgList.append(img_orig)

# Flip Horizontal
imgList.append(img_orig[:,::-1,:])
# Flip Vertical
imgList.append(img_orig[::-1,:,:])
# Flip color channels
imgList.append(img_orig[:,:,::-1])

# Blur flattens original image
blur_img_bw = ndimage.gaussian_filter(img_orig, sigma=4)
imgList.append(blur_img_bw)

# Add blur to color image by blurring each channel
blurry_img = np.zeros_like(img_orig)
for i in range(3):
    blurry_img[:,:,i] = ndimage.gaussian_filter(img_orig[:,:,i], sigma=4)
imgList.append(blurry_img)

# Different blurrs to different channels
blurry_img2 = np.zeros_like(img_orig)
for i in range(3):
    blurry_img2[:,:,i] = ndimage.gaussian_filter(img_orig[:,:,i], sigma=i*10)
imgList.append(blurry_img2)

# Median Filter
median_img = np.zeros_like(img_orig)
for i in range(3):
    median_img[:,:,i] = ndimage.median_filter(img_orig[:,:,i], size=5)
imgList.append(median_img)

# Per-channel Threshold
threshold_img = img_orig * (img_orig > 127)
imgList.append(threshold_img)


import math
nImgs = math.ceil(len(imgList) / 3)

# Display all images
fig, alist = plt.subplots(ncols=3, nrows=nImgs)
rv = alist.ravel()

for i in range(nImgs*3):
    rv[i].axis('off')
    if(i < len(imgList)):
        rv[i].imshow(imgList[i])

plt.tight_layout(pad=0.0)