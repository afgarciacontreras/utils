# -*- coding: utf-8 -*-
import wave
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

in_name = "piano2.wav"

with wave.open(in_name, 'r') as spf:
    (nChannels, ampWidth, sampleRate, nFrames, ctype, cname) = spf.getparams()
    audio_data = spf.readframes(nFrames*nChannels)
    audio_array = np.frombuffer(audio_data, dtype=np.int16)
    left_audio = audio_array[0::2]
    right_audio = audio_array[1::2]
    max_left = np.max(np.abs(left_audio))
    noisy_left = left_audio + (np.random.random(size=left_audio.size)*300)
    noisy_right = right_audio + (np.random.random(size=right_audio.size)*300)
    
    noisy_audio = np.zeros_like(audio_array)
    noisy_audio[0::2] = noisy_left
    noisy_audio[1::2] = noisy_right
    outfile = wave.open('piano_noisy.wav', 'w')
    outfile.setparams((nChannels, ampWidth, sampleRate, nFrames, ctype, cname))
    outfile.writeframes(noisy_audio.tobytes(order='C'))
    outfile.close()
    
    '''
    fig, (axl, axr) = plt.subplots(ncols=2)
    axl.plot(left_audio)
    axr.plot(right_audio)
    plt.show()
    '''
    # Create the filter
    b, a = signal.butter(5, 0.05, btype='lowpass')
    left_filtered = signal.filtfilt(b, a, left_audio)
    right_filtered = signal.filtfilt(b, a, right_audio)
    '''
    fig, (axl, axr) = plt.subplots(ncols=2)
    axl.plot(left_audio)
    axr.plot(left_filtered)
    plt.show()
    '''
    filtered_audio = np.zeros_like(audio_array)
    filtered_audio[0::2] = left_filtered
    filtered_audio[1::2] = right_filtered
    
    outfile = wave.open('piano_filtered.wav', 'w')
    outfile.setparams((nChannels, ampWidth, sampleRate, nFrames, ctype, cname))
    outfile.writeframes(filtered_audio.tobytes(order='C'))
    outfile.close()