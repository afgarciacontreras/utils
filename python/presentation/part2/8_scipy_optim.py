# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize

# Curve-fitting
x = np.linspace(0,5,30)
# Noisy data
y = 0.5 * np.exp(0.8 * x) + np.random.random(30)
# Test Function
def test_f(x, a, b):
    return a * np.exp(b * x)
params, cov = optimize.curve_fit(test_f, x, y, p0=[0.0,1.0])
print(params)
newX = np.linspace(0,5,50)
newY = test_f(newX, params[0], params[1])
plt.plot(x, y, 'o', newX, newY, '-')
plt.show()

# Minimization
def rastrigin(x):
    return 10 + x ** 2 - 10 * np.cos(2 * np.pi * x)
x = np.linspace(-5, 5, 100)
y = rastrigin(x)
plt.plot(x, y)
plt.show()
result = optimize.minimize(rastrigin, x0=1)
#result = optimize.minimize(rastrigin, x0=1, method='Nelder-Mead')
#result = optimize.minimize(rastrigin, x0=1, method='Powell')
#result = optimize.minimize(rastrigin, x0=1, method='COBYLA')
print(result.fun)


# Find zeros
def rastr2(x):
    return rastrigin(x) - 10
y = rastr2(x)
plt.plot(x, y)
plt.show()
print(optimize.newton(rastr2, 1))
