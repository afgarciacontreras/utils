# -*- coding: utf-8 -*-
import numpy as np

# Reading and Writing from files
from_file_1 = np.loadtxt('sample_data.txt')
print(from_file_1)
to_file_1 = np.around(np.random.random((5,5)) * 5, 3)
np.savetxt('sample_data2.txt', to_file_1, fmt='%.3f')

# nparray attributes
print(from_file_1.ndim) # number of dimensions
print(from_file_1.shape) # size of each dimension
print(from_file_1.size) # size of entire array
print(from_file_1.dtype) # type of data in array
print(from_file_1.itemsize) # byte size of 1 elem
print(from_file_1.nbytes) # total byte size of arr
print(from_file_1.T) # traspose of the array

x = from_file_1

# BASIC INDEXING
print(x) # x is a 2D array
print(x[0]) # Entire first row
print(x[1][1]) # Single Element, Python Syntax
print(x[1, 1]) # Single Element, MATLAB Syntax
# SLICING
print(x[0:2]) # Whole rows, 2 is non-inclusive
y = x[1:3,1:3] # sub-array, 2 x 2
print(y)
# Slices are VIEWS, not COPIES
x[1:3,1:3] = np.zeros((2,2))
print(y) # Reflects changes in x

# INDEXING THROUGH lists/arrays
y = np.linspace(1,5,10)
a = [3,3,5,5] # A list of indices in y
print(y)
print(y[a]) # Array with at locations 3, 3, 5 and 5
a = [[0, 4, 7], [0, 4, 1]] # 2D list of indices
print(x[a]) # locations (0,0), (4,4), (7,1)

# BOOLEAN INDEXING
# Returns a boolean array, same size as x
# d[i,j] = x[i,j] > 5
d = x > 5
print(d)
print(x[d]) # 1D list of elements
print(np.any(x < 1.0))
print(np.all(x < 1.0))

