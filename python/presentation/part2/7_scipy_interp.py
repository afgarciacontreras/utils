# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate

# INTERPOLATION
# Original values
x = np.arange(7)
y = np.array([0,0.84,0.9,0.14,-0.75,-0.95,-0.27])
plt.plot(x, y, 'o')
plt.show()

# Linear Interpolation
f = interpolate.interp1d(x, y)
xnew = np.arange(0, 6, 0.1)
ynew = f(xnew)
plt.plot(x, y, 'o', xnew, ynew, '-')
plt.show()

# Quadratic Interpolation
f = interpolate.interp1d(x, y, kind='quadratic')
xnew = np.arange(0, 6, 0.1)
ynew = f(xnew)
plt.plot(x, y, 'o', xnew, ynew, '-')
plt.show()

# Lagrange interpolating polynomial
f = interpolate.lagrange(x, y)
xnew = np.arange(0, 6, 0.1)
ynew = f(xnew)
plt.plot(x, y, 'o', xnew, ynew, '-')
plt.show()

