# -*- coding: utf-8 -*-
import numpy as np

# Some Array Functions
x = np.random.random((3,3)) 
x = np.around(x * 10, 2) - 5
print(x)
print(np.floor(x))
print(np.negative(x))
print(np.mod(x,3))
# Logarithmic
x = np.abs(x)
print(np.log(x))
print(np.exp(x))
print(np.sqrt(x))

# Trigonometric functions
print(np.sin(x))
print(np.cos(x))
print(np.tan(x))
print(np.arctan(x))
print(np.deg2rad(x))
print(np.rad2deg(x))


# Comparison
x = np.random.random(4)
y = np.random.random(4)
print(x)
print(y)
print(x > y)
print(x == y)
print(np.isclose(x,y,rtol=0.75))
print(np.allclose(x,y,rtol=0.75))
print(np.minimum(x,y))
print(np.maximum(x,y))

# Data methods
x = np.random.random(6) * 10
x = np.floor(x)
print(x)
# Searching
print(np.sort(x))
y = np.isin([2,3,5,7], x)
print(y)
print(np.where(x > 4))
# Find indices to maintain order.
# Doesn't work well if array is not sorted
print(np.searchsorted(x,4))
print(np.searchsorted(x,0))
print(np.searchsorted(x,10))

