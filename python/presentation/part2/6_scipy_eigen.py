# -*- coding: utf-8 -*-
import numpy as np
from scipy import linalg as sla


x = np.array([[1,1,1],[0,2,5],[2,5,-1]])
# Determinant
print(sla.det(x))
# Norm
print(sla.norm(x))
# Inverse
print(sla.inv(x))
# Solving a system of equations
# this is the rhs
y = np.array([6,-4,27])
print(sla.solve(x,y))

# Least Squares
x = np.array([[1,2],[2,4],[1,6]])
y = np.array([4,1,3])
# Method returns: 
# solution, residues, rank, 
# singular values
print(sla.lstsq(x,y))

print()

#Eigenvalues
x = np.array([[0,1],[-2,-3]])
# Eigenvalues
print(sla.eigvals(x))
# Solve EV problem
# Returns Eigenvalues & Eigenvectors
print(sla.eig(x))

# Decomposition
y = np.zeros((4,2))
y[0:2,:] = np.array([[2,4],[1,3]])
# Orthonormal basis
print(sla.orth(y))
# Singular Value Decomposition
print(sla.svd(y))
# Polar decomposition
print(sla.polar(y))

