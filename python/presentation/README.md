# PRESENTATION

I worked on two presentations for the local chapter of SIAM, introducing various Python concepts.

The first presentation is [Python: A Beginner's Guide](https://drive.google.com/open?id=1gMpS4wC3y9GVeBB3mOnNry7Z2BeGzJNRZTmd_KNjzlE).

The second presentation is [Python, Numerical Analysis & You](https://drive.google.com/open?id=1k7o4hs4jX5TVXiHk3itV6ivlL3Em6UDL-pcTXZ8ZD5g).