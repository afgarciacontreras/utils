# -*- coding: utf-8 -*-
#!/usr/bin/python

'''
ChooseParticipants.py

A small script designed to select students for in-class participation.

When run, this file will ask for a CSV input file with students and in-class status
(Whether they are "Present" or not, on the last column of each row)

All present students are then shuffled, and the user must choose how many students
will be selected from the list, as well as how many students will be shown at a time.

For example, if the assignment has 5 questions and 2 students have to go up the
whiteboard to answer each question, then the user can choose 10 students, with
2 students at a time.

The main window will display the number of students at a time, cycling through each
set until running out of total students.

'''

# START IMPORTS
import csv
import random
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
# END IMPORTS

'''
Method:     chooseFile()
Input:      None
Output:     filename (str)
'''
def chooseFile():
    return filedialog.askopenfilename(filetypes =(("CSV File", "*.csv"),("All Files","*.*")))

'''
Method:     getParticipants()
Input:      filename (str)
Output:     participants (list)
'''
def getParticipants(filename):
    #3. Create empty list named students
    students = list()
    #4. Open file to variable f
    with open(filename, 'r') as f:
        csv_reader = csv.reader(f)
        #5. For each line:
        for row in csv_reader:
            # a. If student is present, add name to students
            if len(row) > 0 and row[-1] == "Present":
                [lname, fname] = row[0].title().split(", ")
                students.append(fname + " " + lname)            
    #6. Close f
    #7. Randomize the list of students
    random.shuffle(students)
    return students

'''
Method:     getNumberOfParticipants()
Input:      present (int)
Output:     n (int)
'''
def getNumberOfParticipants():
    '''
    8. Create first input window (call method?):
        -------------------------------------
        | # of students at a time? | txtbox |
        -------------------------------------
        | btnOK | btnCancel |               |
        -------------------------------------
    '''
    entries = [0,0]
    top = tk.Tk()
    lblTotal = tk.Label(master=top, text='How many students, in total?', justify=tk.LEFT)
    lblTotal.grid(row=0, column=0, ipadx=10, ipady=5,sticky=tk.W)
    txtTotal = tk.Entry(master=top, justify=tk.RIGHT)
    txtTotal.grid(row=0, column=1, sticky=tk.E)
    lblTime = tk.Label(master=top, text='How many students at a time?', justify=tk.LEFT)
    lblTime.grid(row=1, column=0, ipadx=10, ipady=5, sticky=tk.W)
    txtTime = tk.Entry(master=top, justify=tk.RIGHT)
    txtTime.grid(row=1, column=1, sticky=tk.E)
    def btnCommand():
        entries[0] = int(txtTotal.get())
        entries[1] = int(txtTime.get())
        top.quit()
    btnOK = tk.Button(master=top, text='OK', command=btnCommand)
    btnOK.grid(row=2, column=1, sticky=tk.E)
    top.mainloop()
    top.destroy()
    return entries[0], entries[1]

def runMainWindow(students, numSelected, numPerTurn):
    '''
    8. Based on the # of students, create Participants Window
        -------------------------------------
        | CURRENT PARTICIPANTS         | btnNext |
        -------------------------------------
        | student name 1                    |
        | student name 2                    |
        |   .                               |
        |   .                               |
        |   .                               |
        | student name n-1                  |
        | student name n                    |
        -------------------------------------
    '''
    newStudents = students if numSelected > len(students) else students[:numSelected]
    isOver = [False]
    top = tk.Tk()
    headerText = 'CURRENT PARTICIPANT' + ('S' if numPerTurn > 1 else '')
    lblCurrent = tk.Label(master=top, text=headerText, justify=tk.CENTER)
    lblCurrent['font']=('fixedsys', 24, 'bold')
    lblStudents = list()
    for i in range(numPerTurn):
        lblStudents.append(tk.Label(master=top, text='---', justify=tk.CENTER))
    btnNext = tk.Button(master=top, text='NEXT >>', justify=tk.CENTER)
    btnNext['font']=('fixedsys', 24, 'bold')
    lblCurrent.grid(row=0, column=0, ipadx=20, ipady=2)
    btnNext.grid(row=0, column=1, ipadx=20, ipady=2)
    for i,lblS in enumerate(lblStudents):
        lblS.grid(row=(i+1), column=0, columnspan=2)
        lblS['font'] = ('Arial', 44)
        
    def updateList():
        if isOver[0]:
            top.quit()
        for i,lblS in enumerate(lblStudents):
            if newStudents:
                sName = newStudents.pop()
                lblS['text'] = sName
            else:
                lblS['text'] = '---'
        if not newStudents:
            btnNext['text'] = 'QUIT'
            isOver[0] = True
    
    btnNext['command'] = updateList
    top.mainloop()
    top.destroy()

'''
MAIN ALGORITHM
    1. Open file chooser(?) on same location
    2. Assign chosen file to variable fname
    3. Create empty list named students
    4. Open file to variable f
    5. For each line:
        a. If student is present, add name to students
    6. Close f
    7. Randomize the list of students
    8. Create first input window (call method?):
        -------------------------------------
        | # of students at a time? | txtbox |
        -------------------------------------
        | btnOK | btnCancel |               |
        -------------------------------------
    8. Based on the # of students, create Participants Window
        -------------------------------------
        | CURRENT PARTICIPANTS         | btnNext |
        -------------------------------------
        | student name 1                    |
        | student name 2                    |
        |   .                               |
        |   .                               |
        |   .                               |
        | student name n-1                  |
        | student name n                    |
        -------------------------------------
'''
if __name__ == "__main__":
    # 1. Open file chooser(?) on same location
    # 2. Assign chosen file to variable fname
    participantsFilename = chooseFile()
    if participantsFilename == "":
        messagebox.showinfo(title="INFO",message="No file selected.")
    else:
		# Obtain list of possible participants
        participants = getParticipants(participantsFilename)
		# Obtain total # of participants, # of participants at a time
        totalNum, perTurn = getNumberOfParticipants()
		# Run main Participants window
        runMainWindow(participants, totalNum, perTurn)