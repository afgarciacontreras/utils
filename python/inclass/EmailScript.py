# -*- coding: utf-8 -*-
"""
EmailScript.py

Mass email utility.

Reads a CSV with each student name, email and grades report,
then sends each student their grade via email.

Has various fail-safes due to mass-email restrictions in the server.

This is used with the instructor's authorization.

"""
import time
import smtplib
from email.mime.text import MIMEText
from getpass import getpass

import csv
#6
email_subject = input('Email Subject: ')
email_body_t = ''

username = input('USERNAME: ')
#print('password for :' + username)
password = getpass('PASSWORD: ')

while True:
	try:
		s = smtplib.SMTP('pod51010.outlook.com:587')
		s.starttls()
		ehlo_s = s.ehlo()
		s.login(username, password)
		break
	except smtplib.SMTPAuthenticationError as smtpErr:
		print("Authentication Error! Retrying in 5...")
		time.sleep(10.0)

csvfile = open("grades.csv","r")
reader = csv.DictReader(csvfile)

counter = 0
for row in reader:
	if len(row["PRINTOUT"]) > 0:
		time.sleep(0.6)
		if counter == 10:
			s.quit()
			time.sleep(0.6)
			while True:
				try:
					s = smtplib.SMTP('pod51010.outlook.com:587')
					s.starttls()
					ehlo_s = s.ehlo()
					s.login(username, password)
					break
				except smtplib.SMTPAuthenticationError as smtpErr:
					print("Authentication Error! Retrying in 5...")
					time.sleep(5.0)
			counter = 0
		name = row["NAME"]
		email = row["EMAIL"]
		email_body = row["PRINTOUT"]
		msg = MIMEText(email_body)
		msg['Subject'] = email_subject
		msg['From'] = username
		msg['To'] = email
		#print(msg.as_string())
		#break
		try:
			s.sendmail(username,
					   [email],
					   msg.as_string())
		except smtplib.SMTPServerDisconnected as e:
			time.sleep(1.2)
			while True:
				try:
					s = smtplib.SMTP('pod51010.outlook.com:587')
					s.starttls()
					ehlo_s = s.ehlo()
					s.login(username, password)
					break
				except smtplib.SMTPAuthenticationError as smtpErr:
					print("Authentication Error! Retrying in 5...")
					time.sleep(5.0)
			s.sendmail(username,
					   [email],
					   msg.as_string())
		print(name + ' . . . OK!')
		counter = counter + 1

s.quit()
print('-= COMPLETE =-')