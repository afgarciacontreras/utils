import os
import glob
import zipfile

# List all zip files in current folder
dirs_list = glob.glob("./*.zip")
print(dirs_list)

# for each zip file...
for dir in dirs_list:
	#extract the file to a directory w/the same name
	try:
		with zipfile.ZipFile(dir, 'r') as zFile:
			newdir = dir.replace(".zip", "\\")
			zFile.extractall(newdir)
		#delete the zip file
		os.remove(dir)
	except Exception:
		print("Error with file " + dir)