local anime_mod = RegisterMod("The Orbital Laser", 1)
local orbital_trinket = Isaac.GetTrinketIdByName("Orbital Laser")
local skybeam_id = Isaac.GetEntityVariantByName("Sky Beam 2")

local skybeam_eff = nil

-- Comput a Unit Vector representing the wall's rebound
function anime_mod:GetWallNormal(room, position)
  local clamped_pos = room:GetClampedPosition(position,0.0)
  local norm_X, norm_Y = 0, 0
  local diff_X = clamped_pos.X - position.X
  local diff_Y = clamped_pos.Y - position.Y

  if diff_X > 0 then
    norm_X = 1
  elseif diff_X < 0 then
    norm_X = -1
  end 

  if diff_Y > 0 then
    norm_Y = 1
  elseif diff_Y < 0 then
    norm_Y = -1
  end

  return Vector(norm_X,norm_Y):Resized(1.0)
end

-- Compute rebound Velocity.
-- A velocity reduction would multiply the result by n < 1.0
function anime_mod:ReboundVelocity(velocity,normal)
  return (normal * (-2 * velocity:Dot(normal))) + velocity
end


function anime_mod:spawn_beam()
  local player = Isaac.GetPlayer(0)
  -- There can only be one Orbital Laser beam
  if player:HasTrinket(orbital_trinket) and skybeam_eff == nil then
    -- A random position 30 units away from Isaac
    local pos = player.Position + Vector(0,30):Rotated(player:GetTrinketRNG(orbital_trinket):RandomInt(359))
    -- If it is outside of the Room, compute position inside room
    pos = Game():GetLevel():GetCurrentRoom():GetClampedPosition(pos,0.0)
    -- Velocity is between 5-6
    local base_velocity = 5 + player:GetTrinketRNG(orbital_trinket):RandomFloat()
    -- Direction is random
    local beam_velocity = Vector(0,base_velocity):Rotated(player:GetTrinketRNG(orbital_trinket):RandomInt(359))
    local beam = Isaac.Spawn(EntityType.ENTITY_EFFECT, skybeam_id, 0, pos, beam_velocity, player)
    skybeam_eff = beam:ToEffect()
    -- SetTimeout changes 2 values: LifeSpan & Timeout.
    -- Timeout is reduced each tick
    skybeam_eff:SetTimeout(75)
  end
end

function anime_mod:effect_update()
  if skybeam_eff ~= nil then
    -- The Beam bounces off walls
    local the_room = Game():GetLevel():GetCurrentRoom()
    local this_grid_ent = the_room:GetGridCollisionAtPos(skybeam_eff.Position)
    -- Detect if in wall
    if this_grid_ent == GridCollisionClass.COLLISION_WALL then
      -- Compute new velocity (rebound, no loss of vel)
      local normal = anime_mod:GetWallNormal(the_room, skybeam_eff.Position)
      local new_vel = anime_mod:ReboundVelocity(skybeam_eff.Velocity, normal)
      skybeam_eff.Velocity = new_vel
    end
    -- Beam has no contact damage, so we must compute based on existing data
    local entList = Isaac.GetRoomEntities()
    local beamRadius = skybeam_eff.Size
    for i=1,#entList do
      if entList[i]:IsVulnerableEnemy() then
        local entRadius = entList[i].Size
        local dist2touch = entRadius + beamRadius
        local dist_between = skybeam_eff.Position:Distance(entList[i].Position, entList[i].Position)
        -- Beam touches enemy only if the distance between their centers is less than their combined radii
        -- Beam only damages every 3 ticks
        if dist_between <= dist2touch and skybeam_eff:IsFrame(3, 0) then
          entList[i]:TakeDamage(8.0, DamageFlag.DAMAGE_LASER, EntityRef(Isaac.GetPlayer(0)), 0)
        end
      end
    end
    -- Create visual effect. No damage, but looks cool.
    if skybeam_eff:IsFrame(3,0) then
      local drop = Isaac.Spawn(EntityType.ENTITY_EFFECT, 
                               EffectVariant.CROSS_POOF, 0, 
                               skybeam_eff.Position, 
                               Vector(0,0), 
                               Isaac.GetPlayer(0))
      drop.SpriteScale = Vector(0.8,0.5)
    end
    
    -- Animation control
    if skybeam_eff.Timeout > skybeam_eff.LifeSpan - 8 then
      skybeam_eff:SetSpriteFrame("Begin", skybeam_eff.LifeSpan - skybeam_eff.Timeout)
    elseif skybeam_eff.Timeout > 7 then
      skybeam_eff:SetSpriteFrame("Spotlight", (skybeam_eff.LifeSpan - skybeam_eff.Timeout) % 10)
    elseif skybeam_eff.Timeout > 1 then
      skybeam_eff:SetSpriteFrame("End", 7 - skybeam_eff.Timeout)
    else
      skybeam_eff:Remove()
      skybeam_eff = nil
    end
  end
end



anime_mod:AddCallback(ModCallbacks.MC_POST_UPDATE, anime_mod.effect_update)
anime_mod:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, anime_mod.spawn_beam, EntityType.ENTITY_PLAYER)