local anime_mod = RegisterMod("Anime-Inspired Drill", 1)
local visor_item = Isaac.GetItemIdByName("Drill Pendant")
local familiar_id = Isaac.GetEntityVariantByName("Drill Familiar")


local visor_costume = Isaac.GetCostumeIdByPath("gfx/characters/anime_visor.anm2")

-- Drill Data
local drill_base_dmg = 3.0

-- Drill movement help arrays
local dir2Vector = {[0] = Vector(-1,0), [1] = Vector(0,-1), [2] = Vector(1,0), [3] = Vector(0,1)}
local dirName = {[0] = "Side", [2] = "Side",[1] = "Up", [3] = "Down"}
local fxName = {[0] = "FxRight", [2] = "FxRight",[1] = "FxUp", [3] = "FxDown"}

function anime_mod:player_update()
  local player = Isaac.GetPlayer(0)

  if player:HasCollectible(visor_item) then

    local ents = Isaac.GetRoomEntities()
    local temp_entity = nil
    -- Look for existing drill
    for i=1,#ents do
      if ents[i].Type == EntityType.ENTITY_FAMILIAR and ents[i].Variant == familiar_id then
        temp_entity = ents[i]
        break
      end
    end

    -- If existing drill not found, spawn one
    -- ( Used for reloading )
    if temp_entity == nil then
      temp_entity = Isaac.Spawn(EntityType.ENTITY_FAMILIAR, familiar_id, 0, player.Position, Vector(0,0), player:GetLastChild())
      player:AddNullCostume(visor_costume)
    end
  else
    local ents = Isaac.GetRoomEntities()
    for i=1,#ents do
      if ents[i].Type == EntityType.ENTITY_FAMILIAR and ents[i].Variant == familiar_id then
        ents[i]:Remove()
        break
      end
    end
  end
end

function anime_mod:player_init(player)
  if player:HasCollectible(visor_item) then
    player:AddNullCostume(visor_costume)
  end
end

function anime_mod:player_damaged(player, amount, dmgFlag, dmgSrc, dmgCountdown)
    local ents = Isaac.GetRoomEntities()
    for i=1,#ents do
      if ents[i].Type == EntityType.ENTITY_FAMILIAR and ents[i].Variant == familiar_id then
        local famDrill = ents[i]:ToFamiliar()
        if famDrill.RoomClearCount < 6 then
          famDrill.RoomClearCount = famDrill.RoomClearCount + 1
        end
        break
      end
    end
end

function anime_mod:familiar_init(drill_entity)
  
  local drill_familiar = drill_entity:ToFamiliar()
  
  -- If New Floor, reset stats
  local levelInfo = Game():GetLevel()
  if (levelInfo:GetPreviousRoomIndex() == levelInfo:GetCurrentRoomIndex() and
      levelInfo:GetCurrentRoomIndex()== levelInfo:GetStartingRoomIndex()) then
    drill_familiar.RoomClearCount = 0
  end
  
  drill_familiar:FollowParent()
  drill_familiar:SetSpriteFrame("Float", Isaac.GetFrameCount() % 32)
  drill_familiar.CollisionDamage = 0.0
  drill_familiar.FireCooldown = 0
end

function anime_mod:familiar_update(drill_entity)
  local player = Isaac.GetPlayer(0)
  local drill_familiar = drill_entity:ToFamiliar()
  
  -- If New Floor, reset stats
  local levelInfo = Game():GetLevel()
  if (levelInfo:GetPreviousRoomIndex() == levelInfo:GetCurrentRoomIndex() and
      levelInfo:GetCurrentRoomIndex()== levelInfo:GetStartingRoomIndex()) then
    drill_familiar.RoomClearCount = 0
  end
  -- Drill Bit Behavior
  local final_dmg = 0
  local drill_level = 1

  if drill_familiar.RoomClearCount > 3 then
    drill_level = 2
  end
  
  final_dmg = drill_base_dmg * 1.3^drill_familiar.RoomClearCount
  
  if drill_familiar.FireCooldown == 0 then -- If Drill is ready to shoot...
    if math.floor(player:GetLastActionTriggers() / ActionTriggers.ACTIONTRIGGER_SHOOTING) % 2 == 1 then -- ... and player is shooting...
      local p_dir_shot = player:GetFireDirection()
      local base_shot_v = dir2Vector[p_dir_shot] * 10 -- sets base flight
      local shot_dev = nil
      if base_shot_v.X == 0 then -- vertical shooting
        shot_dev = Vector(player.Velocity.X / math.abs(player.Velocity.X), 0)
      else -- horizontal shooting
        shot_dev = Vector(0, player.Velocity.Y / math.abs(player.Velocity.Y))
      end
      local drill_velocity = Vector(base_shot_v.X, base_shot_v.Y)
      drill_familiar.Velocity = drill_velocity

      drill_familiar.CollisionDamage = final_dmg
      drill_familiar.FireCooldown = -1
      drill_familiar.State = p_dir_shot
    else 
      drill_familiar:FollowParent()
      if drill_level == 1 then
        drill_familiar:SetSpriteFrame("Float", Isaac.GetFrameCount() % 32)
        drill_familiar:SetSpriteOverlayFrame("Float", Isaac.GetFrameCount() % 32)
      else
        drill_familiar:SetSpriteFrame("Float2", Isaac.GetFrameCount() % 32)
        drill_familiar:SetSpriteOverlayFrame("Float2", Isaac.GetFrameCount() % 32)
      end
    end
  elseif drill_familiar.FireCooldown == -1 then -- If Drill is in-flight...
    local drill_curr_dir = drill_familiar.State
    local anim_name = "Shoot"..dirName[drill_curr_dir]
    if drill_level > 1 then
      anim_name = anim_name.."2"
    end
    
    if drill_curr_dir == Direction.LEFT then
      drill_familiar.FlipX = true
    else
      drill_familiar.FlipX = false
    end
    
    local overlay_name = fxName[drill_curr_dir]

    drill_familiar:SetSpriteFrame(anim_name, Isaac.GetFrameCount() % 6)
    drill_familiar:SetSpriteOverlayFrame(overlay_name, Isaac.GetFrameCount() % 6)

    local collision_id = Game():GetRoom():GetGridCollisionAtPos(drill_familiar.Position)

    if not (Game():GetRoom():IsPositionInRoom(drill_familiar.Position,0.15) and collision_id < 2) then
      -- If drill can destroy rocks, destroy them
      if drill_level > 1 then
        local gridEnt = Game():GetRoom():GetGridEntityFromPos(drill_familiar.Position)
        local gridEntType = gridEnt.Desc.Type
        if (gridEntType == GridEntityType.GRID_ROCK or
            gridEntType == GridEntityType.GRID_ROCKT or 
            gridEntType == GridEntityType.GRID_ROCK_BOMB or
            gridEntType == GridEntityType.GRID_ROCK_ALT or 
            gridEntType == GridEntityType.GRID_TNT or
            gridEntType == GridEntityType.GRID_POOP) then
          gridEnt:Destroy(true)
        else 
          drill_familiar.FireCooldown = -7
          drill_familiar.Velocity = Vector(0,0)
        end
      else
        drill_familiar.FireCooldown = -7
        drill_familiar.Velocity = Vector(0,0)
      end
    end
  elseif drill_familiar.FireCooldown <= -2 then
    local drill_curr_dir = drill_familiar.State
    local anim_name = "Shoot"..dirName[drill_curr_dir]
    
    if drill_level > 1 then
      anim_name = anim_name.."2"
    end

    drill_familiar.CollisionDamage = 0.0
    if drill_curr_dir == Direction.LEFT then
      drill_familiar.FlipX = true
    else
      drill_familiar.FlipX = false
    end

    local overlay_name = fxName[drill_curr_dir]

    drill_familiar:SetSpriteFrame(anim_name, Isaac.GetFrameCount() % 6)
    drill_familiar:SetSpriteOverlayFrame(overlay_name, Isaac.GetFrameCount() % 6)

    if drill_familiar.FireCooldown == -2 then
      drill_familiar.FireCooldown = 35
    else
      drill_familiar.FireCooldown = drill_familiar.FireCooldown + 1
    end

  else
    drill_familiar:FollowParent()
    if drill_level == 1 then
      drill_familiar:SetSpriteFrame("Float", Isaac.GetFrameCount() % 32)
      drill_familiar:SetSpriteOverlayFrame("Float", Isaac.GetFrameCount() % 32)
    else
      drill_familiar:SetSpriteFrame("Float2", Isaac.GetFrameCount() % 32)
      drill_familiar:SetSpriteOverlayFrame("Float2", Isaac.GetFrameCount() % 32)
    end
    drill_familiar.FireCooldown = drill_familiar.FireCooldown - 1
  end
end

anime_mod:AddCallback(ModCallbacks.MC_POST_UPDATE , anime_mod.player_update, EntityType.ENTITY_PLAYER)
anime_mod:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, anime_mod.player_init, EntityType.ENTITY_PLAYER)
anime_mod:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, anime_mod.player_damaged, EntityType.ENTITY_PLAYER)

anime_mod:AddCallback(ModCallbacks.MC_FAMILIAR_INIT, anime_mod.familiar_init, familiar_id)
anime_mod:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE, anime_mod.familiar_update, familiar_id)