-- Register mod
local voyMod = RegisterMod("Voyeurism", 1.0);
local challengeId = Isaac.GetChallengeIdByName("The Voyeur")

local cooldown = -1
local jump_start = Vector(0,0)
local cntdown_to_peep = -1

function voyMod:GameUpdate()
  if Isaac.GetChallenge() == challengeId then

    local player = Isaac.GetPlayer(0)

    local htj_effect2 = player:GetEffects():GetCollectibleEffect(CollectibleType.COLLECTIBLE_HOW_TO_JUMP)
    if htj_effect2 == nil then
      cooldown = -1
      local move_vector = player:GetShootingJoystick()
      if move_vector:Length() > 0.0 then
        player:AddVelocity(move_vector)
        player:UseActiveItem(CollectibleType.COLLECTIBLE_HOW_TO_JUMP, true, false, false, false)
      end
    else
      if cooldown == -1 then
        jump_start = player.Position
      end
      cooldown = htj_effect2.Cooldown
    end

    if cooldown == 1 then
      local rng = RNG()
      local isaac_loc = player.Position
      local tearSp = player.ShotSpeed
      local distance = math.sqrt((jump_start.X - isaac_loc.X)^2 +(jump_start.Y - isaac_loc.Y)^2)
      local angle_step = 45
      local angle_init = 0

      if distance < 20 then
        angle_step = 36
        angle_init = math.random(0,angle_step-1)
      end

      for r = angle_init,360-1,angle_step do
        local tear = player:FireTear(isaac_loc, Vector(player.TearHeight*player.ShotSpeed/2.5,0):Rotated(r),false, false, false)
      end
      cooldown = 2
      local blood_ent = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.PLAYER_CREEP_RED, 0, isaac_loc, Vector(0,0), player)
      local blood_eff = blood_ent:ToEffect()
      blood_eff:SetTimeout(math.floor(-player.TearHeight/1.5))
      blood_eff.Scale = 2.5 * player.ShotSpeed
      blood_eff.CollisionDamage = (10/player.MaxFireDelay) * player.Damage/2.0
      -- last moment of jump
      cooldown = cooldown - 1
    end
    
    if cntdown_to_peep > 0 then
      cntdown_to_peep = cntdown_to_peep - 1
      if cntdown_to_peep < 5 then
        local game = Game()
        local room = game:GetRoom()
        game:ShakeScreen(10)
      end      
    elseif cntdown_to_peep == 0 then
      local game = Game()
      local room = game:GetRoom()
      Isaac.Spawn(EntityType.ENTITY_PEEP, 1, 1, room:GetCenterPos(), Vector(0,0), nil)
      Isaac.Spawn(EntityType.ENTITY_PEEP, 1, 2, room:GetCenterPos(), Vector(0,0), nil)
      cntdown_to_peep = -1
    end
  end
end

function voyMod:momDamaged(dmgdEntity, dmgAmnt, dmgFlag, dmgSrc, dmgCntDwn)
  if dmgdEntity.HitPoints <= dmgAmnt then
    cntdown_to_peep = 20
  end

end


voyMod:AddCallback(ModCallbacks.MC_POST_UPDATE, voyMod.GameUpdate)
voyMod:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, voyMod.momDamaged, EntityType.ENTITY_MOM)