# utils

This is a repository of various small projects / codes I have worked since around 2017. It is not everything I have done, mostly because not everything I have done can be published yet, for intellectual property reasons.

What is publishable, is and will be publishable through this humble repository.

I am separating the repository by language.

Currently there are three main languages:

* _Python_. My forte. Has the most scripts.

* _Java_. We use this for teaching, so of course there is some Java code too. 

* _Lua_. A bit of an anomaly, but included mostly because of a series of hobby projects in modding the game *[The Binding of Isaac](http://bindingofisaac.com/)*.